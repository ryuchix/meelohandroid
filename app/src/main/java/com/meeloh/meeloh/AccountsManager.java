package com.meeloh.meeloh;

import com.meeloh.meeloh.Models.User;

public class AccountsManager {

    private static volatile AccountsManager instance;

    private User user;
    private String passportToken;

    private AccountsManager() {
        if (instance != null) {
            throw new RuntimeException("Use getInstance() method to get the instance");
        }
    }

    public static AccountsManager getInstance(){
        if (instance == null) {
            synchronized (AccountsManager.class) {
                if (instance == null) {
                    instance = new AccountsManager();
                }
            }

        }

        return instance;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPassportToken(String token) {
        passportToken = token;
    }

    public User getUser() { return user; }

    public String getPassportToken() {
        return passportToken;
    }
}
