package com.meeloh.meeloh;

import android.support.annotation.Nullable;

import com.meeloh.meeloh.Models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class UserAccountSetter {

    private User user;
    private JSONObject response;

    public UserAccountSetter() {}

    public UserAccountSetter(JSONObject response) {
        this.response = response;
    }

    public User setupUserAccount() {
        this.user = new User();

        try {
            user.setId(response.getInt("id"));
            user.setmFirstName(response.getString("firstname"));
            user.setmLastName(response.getString("lastname"));
            user.setUsertype(response.getInt("usertype"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return user;

    }

    public void updateUserAccount(HashMap<String, String> requestBody, @Nullable String image) {
        User user = AccountsManager.getInstance().getUser();
        if (image != null) user.setImage(image);

        for (String key : requestBody.keySet()) {
            switch (key) {
                case "firstname":
                    user.setmFirstName(requestBody.get(key));
                    break;
                case "lastname":
                    user.setmLastName(requestBody.get(key));
                    break;
                default:
                    break;
            }
        }
    }

    public void updateImage(@Nullable String image) {
        User user = AccountsManager.getInstance().getUser();
        if (image != null) user.setImage(image);
    }

}
