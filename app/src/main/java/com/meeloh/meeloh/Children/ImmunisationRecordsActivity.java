package com.meeloh.meeloh.Children;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.meeloh.meeloh.R;

public class ImmunisationRecordsActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immunisation_records);

        mToolbar = findViewById(R.id.immu_toolbar);
        mToolbar.setTitle(R.string.immunisation_records);
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
