package com.meeloh.meeloh.Children;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.MainActivity;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewChildActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private boolean clicked = false;
    private LinearLayout birthDetails, immunisationRecords, periodicChecks, illnessRecords, treatmentRecords;
    private TextView mName, mBirthday;
    private CircleImageView mImage;
    private ImageView cImage;
    private int childId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_child);

        mToolbar = findViewById(R.id.view_child_toolbar);
        mToolbar.setTitle("Children");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.children_menu);
        clicked = false;

        View view = navigation.findViewById(R.id.children_menu);
        view.setOnClickListener(this);

        birthDetails = findViewById(R.id.birthdetails_ll);
        birthDetails.setOnClickListener(this);
        immunisationRecords = findViewById(R.id.immu_ll);
        immunisationRecords.setOnClickListener(this);
        periodicChecks = findViewById(R.id.periodiccheck_ll);
        periodicChecks.setOnClickListener(this);
        illnessRecords = findViewById(R.id.illnessrecords_ll);
        illnessRecords.setOnClickListener(this);
        treatmentRecords = findViewById(R.id.treatmentrecords_ll);
        treatmentRecords.setOnClickListener(this);

        mName = findViewById(R.id.name_tv);
        mBirthday = findViewById(R.id.birthday_tv);
        mImage = findViewById(R.id.profile_image);
        cImage = findViewById(R.id.profile_image_iv);

        childId = getIntent().getIntExtra("children_id", 0);

        getChild();
    }

    @Override
    protected void onResume() {
        getChild();
        super.onResume();
    }

    private void getChild() {
        String mUrl = getString(R.string.get_child_url) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setChild(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    @SuppressLint("SetTextI18n")
    private void setChild(JSONObject response) {
        try {
            mName.setText(response.getString("firstname") + " " + response.getString("lastname"));
            mBirthday.setText(response.getString("birthday"));

            Picasso.get().load(response.getString("image")).placeholder(R.drawable.user_default).into(mImage);
            String gender = response.getString("gender");
            if (gender.equals("Male")) {
                mName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.male, 0);
            } else {
                mName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.female, 0);
            }
            if (response.getString("image") != null && !response.getString("image").equals("null")) {
                cImage.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.update_profile_menu) {
            Intent intent = new Intent(this, EditChildActivity.class);
            intent.putExtra("children_id", childId);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressLint("RestrictedApi")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Intent intent;
            switch (item.getItemId()) {
                case R.id.children_menu:
                    return true;
                case R.id.calendar_menu:
                    intent = new Intent(ViewChildActivity.this, MainActivity.class);
                    intent.putExtra("tab", 2);
                    startActivity(intent);
                    return true;
                case R.id.profile_menu:
                    intent = new Intent(ViewChildActivity.this, MainActivity.class);
                    intent.putExtra("tab", 3);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.children_menu:
                openActivity();
                break;
            case R.id.birthdetails_ll:
                openBirthDetails();
                break;
            case R.id.immu_ll:
                openImmunisation();
                break;
            case R.id.periodiccheck_ll:
                openPeriodicCheck();
                break;
            case R.id.illnessrecords_ll:
                openIllnessRecords();
                break;
            case R.id.treatmentrecords_ll:
                openTreatmentRecords();
                break;
        }
    }

    private void openTreatmentRecords() {
        Toast.makeText(this, "Treatment records", Toast.LENGTH_SHORT).show();
    }

    private void openIllnessRecords() {
        Toast.makeText(this, "Illness records", Toast.LENGTH_SHORT).show();
    }

    private void openPeriodicCheck() {
        Intent intent;
        intent = new Intent(this, PeriodicCheckActivity.class);
        startActivity(intent);
    }

    private void openImmunisation() {
        Intent intent;
        intent = new Intent(this, ImmunisationRecordsActivity.class);
        startActivity(intent);
    }

    private void openBirthDetails() {
        Intent intent;
        intent = new Intent(this, BirthDetailsActivity.class);
        startActivity(intent);
    }

    private void openActivity() {
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 1);
        startActivity(intent);
    }
}
