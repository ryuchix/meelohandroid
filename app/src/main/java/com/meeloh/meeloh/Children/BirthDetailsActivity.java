package com.meeloh.meeloh.Children;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.florent37.expansionpanel.ExpansionHeader;
import com.github.florent37.expansionpanel.ExpansionLayout;
import com.meeloh.meeloh.R;

public class BirthDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private TextView mNewbornExam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birth_details);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.birth_details);
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ExpansionLayout maternalExpansionLayout = findViewById(R.id.expansionLayout);
        final ExpansionHeader maternalExpansionHeader = findViewById(R.id.maternal_header);

        ExpansionLayout neonatalExpansionLayout = findViewById(R.id.expansionLayout2);
        final ExpansionHeader neonatalExpansionHeader = findViewById(R.id.neonatal_header);

        ExpansionLayout dischargeExpansionLayout = findViewById(R.id.expansionLayout3);
        final ExpansionHeader dischargeExpansionHeader = findViewById(R.id.discharge_header);

        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) maternalExpansionLayout.getLayoutParams();

        maternalExpansionLayout.addListener(new ExpansionLayout.Listener() {
            @Override
            public void onExpansionChanged(ExpansionLayout expansionLayout, boolean expanded) {
                if (expanded) {
                    maternalExpansionHeader.setBackgroundColor(getColor(R.color.whiteBackground));
                    maternalExpansionHeader.scrollBy(0, 0);

                } else {
                    maternalExpansionHeader.setBackgroundColor(getColor(R.color.mainBackground));
                }
            }
        });

        neonatalExpansionLayout.addListener(new ExpansionLayout.Listener() {
            @Override
            public void onExpansionChanged(ExpansionLayout expansionLayout, boolean expanded) {
                if (expanded) {
                    neonatalExpansionHeader.setBackgroundColor(getColor(R.color.whiteBackground));
                } else {
                    neonatalExpansionHeader.setBackgroundColor(getColor(R.color.mainBackground));
                }
            }
        });

        dischargeExpansionLayout.addListener(new ExpansionLayout.Listener() {
            @Override
            public void onExpansionChanged(ExpansionLayout expansionLayout, boolean expanded) {
                if (expanded) {
                    dischargeExpansionHeader.setBackgroundColor(getColor(R.color.whiteBackground));
                } else {
                    dischargeExpansionHeader.setBackgroundColor(getColor(R.color.mainBackground));

                }
            }
        });

        mNewbornExam = findViewById(R.id.newborn_exam_tv);
        mNewbornExam.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newborn_exam_tv:
                openNewbornExam();
                break;
        }
    }

    private void openNewbornExam() {
        Intent intent = new Intent(this, ViewNewbornExaminationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
