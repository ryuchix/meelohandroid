package com.meeloh.meeloh.Children;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.androidnetworking.error.ANError;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleyMultipartRequestCallback;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditChildActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;

    private String TAG = "EditChildActivity";
    private Button saveChildBtn;
    private LayoutInflater mInflator;
    private Spinner typeSpinner;
    private Boolean selected;
    private HashMap<String , String > requestBody;
    private TextInputEditText mFirstname, mLastname, mPhone, mGender, mBlood;
    private String fname, lname, phone, blood;
    DatePickerDialog picker;
    private TextInputLayout mCalendar;
    private ImageButton mUploadImage;
    private static int REQUEST_READ_STORAGE = 100;
    private int mIsImagePicked = 0;
    private CircleImageView mImage;
    private String mImagePath;
    private Integer childId;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_child);

        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.add_child_toolbar);
        mToolbar.setTitle("Edit your child's details");
        mToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveChildBtn = findViewById(R.id.save_child_btn);
        saveChildBtn.setOnClickListener(this);

        selected = false;
        typeSpinner = (Spinner) findViewById(R.id.spinner);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(typeSelectedListener);
        typeSpinner.setOnTouchListener(typeSpinnerTouchListener);
        mInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        mFirstname = findViewById(R.id.first_name_tiet);
        mLastname = findViewById(R.id.last_name_tiet);
        mPhone = findViewById(R.id.phone_tiet);
        mPhone.setOnClickListener(this);
        mBlood = findViewById(R.id.blood_type_tiet);
        mUploadImage = findViewById(R.id.add_child_image_btn);
        mUploadImage.setOnClickListener(this);
        mImage = findViewById(R.id.profile_image);

        childId = getIntent().getIntExtra("children_id", 0);
        getChild();
    }

    private void getChild() {
        String mUrl = getString(R.string.get_child_url) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setChild(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setChild(JSONObject response) {
        try {
            mFirstname.setText(response.getString("firstname"));
            mLastname.setText(response.getString("lastname"));
            mPhone.setText(response.getString("birthday"));
            mBlood.setText(response.getString("blood_type"));
            Picasso.get().load(response.getString("image")).into(mImage);
            String gender = response.getString("gender");

            if (gender.equals("Male")) {
                typeSpinner.setSelection(0);
            } else {
                typeSpinner.setSelection(1);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private SpinnerAdapter typeSpinnerAdapter = new BaseAdapter() {

        private TextView text;
        private String[] data = { "Male", "Female" };
        private int count = 2;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return count;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            return false;
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_child_btn:
                updateChild();
                break;
            case R.id.phone_tiet:
                openDatePicker();
                break;
            case R.id.add_child_image_btn:
                mIsImagePicked = 1;
                openGallery();
                break;
        }
    }

    private void openDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        final SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        final String month_name = month_date.format(cldr.getTime());
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mPhone.setText(MONTHS[monthOfYear] + " " + dayOfMonth + ", " + year);
                    }
                }, year, month, day);
        picker.show();
    }

    private void updateChild() {

        fname = mFirstname.getText().toString();
        lname = mLastname.getText().toString();
        phone = mPhone.getText().toString();
        blood = mBlood.getText().toString();

        if (fname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mFirstname.requestFocus();
        } else if (lname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLastname.requestFocus();
        } else if (phone.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPhone.requestFocus();
        } else if (blood.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mBlood.requestFocus();
        } else {
            updateDetails();
        }
    }

    private void updateDetails() {
        String mUrl = getString(R.string.update_child_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("id", String.valueOf(childId));
        requestBody.put("firstname", fname);
        requestBody.put("lastname", lname);
        requestBody.put("birthday", phone);
        requestBody.put("gender", typeSpinner.getSelectedItem().toString());
        requestBody.put("blood_type", blood);

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Log.d("dfsdfsdf", String.valueOf(mIsImagePicked));
                        if (mIsImagePicked == 1) {
                            uploadChildImage(childId);
                        }

                        Toast.makeText(EditChildActivity.this, "Child updated.", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    private void uploadChildImage(Integer childID) {
        Log.d("sdfsf", String.valueOf(childID));
        new SendRequest(this).MultipartRequest(getString(R.string.add_child_image_url) + childID,
                "image",
                new File(mImagePath),
                new VolleyMultipartRequestCallback() {
                    @Override
                    public void onNetworSuccessResponse(JSONObject response) {
                        mIsImagePicked = 2;
                    }

                    @Override
                    public void onNetworErrorResponse(ANError error) {

                    }
                });
    }

    private void openGallery() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_READ_STORAGE);
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_READ_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_READ_STORAGE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_READ_STORAGE) {
            if (resultCode == RESULT_OK && data.getData() != null) {
                String imagePath;

                Uri selectedImage = data.getData();
                String[] filePathToColumn = new String[]{MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage, filePathToColumn,
                        null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathToColumn[0]);
                imagePath = cursor.getString(columnIndex);
                cursor.close();

                if (mIsImagePicked == 1) {
                    mImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                    mImagePath = imagePath;
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
