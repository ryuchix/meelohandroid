package com.meeloh.meeloh.Home.Fragments.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.Doctor.DoctorList.DoctorListActivity;
import com.meeloh.meeloh.EditRiskFactorQuestionsActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private JSONObject detailsObject;
    private TextView mName, mPhone, mGender, mBlood, mPName, mPPhone, mPGender, mPBlood, mRisk, mList;
    private User mUser;
    private CircleImageView mImage, mPartnerImage;
    private ImageView cImage, cPartnerImage;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mUser = AccountsManager.getInstance().getUser();

        mName = view.findViewById(R.id.name_tv);
        mPhone = view.findViewById(R.id.phone_tv);
        mGender = view.findViewById(R.id.gender_tv);
        mBlood = view.findViewById(R.id.blood_tv);
        mImage = view.findViewById(R.id.profile_image);
        cImage = view.findViewById(R.id.profile_image_iv);

        mPName = view.findViewById(R.id.pname_tv);
        mPPhone = view.findViewById(R.id.pphone_tv);
        mPGender = view.findViewById(R.id.pgender_tv);
        mPBlood = view.findViewById(R.id.pblood_tv);
        mPartnerImage = view.findViewById(R.id.profile_partner_image);
        cPartnerImage = view.findViewById(R.id.profile_partner_image_iv);

        mRisk = view.findViewById(R.id.risk_factor_tv);
        mRisk.setOnClickListener(this);
        mList = view.findViewById(R.id.list_doctor_tv);
        mList.setOnClickListener(this);

        return view;
    }

    private void getProfile() {
        String mUrl = getString(R.string.get_user_details_url) + mUser.getId();
        new SendRequest(getActivity()).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setprofile(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setprofile(JSONObject response) {
        try {
            mName.setText(response.getString("firstname") + " " + response.getString("lastname"));
            mGender.setText(response.getString("gender"));
            mPhone.setText(response.getString("phone"));
            mBlood.setText(response.getString("blood_type"));
            Picasso.get().load(response.getString("image")).placeholder(R.drawable.user_default).into(mImage);
            if (response.getString("image") != null) {
                cImage.setVisibility(View.INVISIBLE);
            }

            JSONObject partner = response.optJSONObject("partner");
            mPName.setText(partner.getString("firstname") + " " + partner.getString("lastname"));
            mPGender.setText(partner.getString("gender"));
            mPPhone.setText(partner.getString("phone"));
            mPBlood.setText(partner.getString("blood_type"));
            Picasso.get().load(partner.getString("image")).placeholder(R.drawable.user_default).into(mPartnerImage);
            if (partner.getString("image") != null) {
                cPartnerImage.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        getProfile();
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.risk_factor_tv:
                openRisk();
                break;
            case R.id.list_doctor_tv:
                openDoctors();
                break;
        }
    }

    private void openDoctors() {
        Intent intent = new Intent(getActivity(), DoctorListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void openRisk() {
        Intent intent = new Intent(getActivity(), EditRiskFactorQuestionsActivity.class);
        intent.putExtra("userid", mUser.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}


