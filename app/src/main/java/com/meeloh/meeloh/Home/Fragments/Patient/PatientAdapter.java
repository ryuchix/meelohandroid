package com.meeloh.meeloh.Home.Fragments.Patient;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.ChildrenHolder>{
    private Context mContext;
    private List<Object> mListItems;
    private PatientInterface mCallback;

    public PatientAdapter(Context context, List<Object> items, PatientInterface callback) {
        mContext = context;
        mListItems = items;
        mCallback = callback;
    }

    @NonNull
    @Override
    public PatientAdapter.ChildrenHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_patient_list,
                parent, false);
        return new PatientAdapter.ChildrenHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientAdapter.ChildrenHolder holder, final int i) {
        Children child = (Children) mListItems.get(i);

        holder.name.setText(child.getFirstname() + " " + child.getLastname());
        Picasso.get().load(child.getImage()).placeholder(R.drawable.user_default).into(holder.image);

        holder.parent.setText(child.getParentName());
        holder.parent.setText(child.getParentName());

        if (child.getImage() != null && !child.getImage().equals("null")) {
            holder.imageOverlay.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems != null ? mListItems.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ChildrenHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView image;
        TextView name, parent;
        LinearLayout parentll;
        ImageView imageOverlay;

        public ChildrenHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_tv);
            parent = itemView.findViewById(R.id.parent_name_tv);
            parent.setVisibility(View.VISIBLE);
            image = itemView.findViewById(R.id.profile_image);
            parentll = itemView.findViewById(R.id.parent_ll);
            parentll.setOnClickListener(this);
            imageOverlay = itemView.findViewById(R.id.profile_image_iv);
        }

        @Override
        public void onClick(View v) {
            Children children = (Children) mListItems.get(getAdapterPosition());

            switch (v.getId()) {
                case R.id.parent_ll: {
                    mCallback.onViewPatient(children.getId(), children.getChildEnrolled());
                    break;
                }
            }
        }
    }

}
