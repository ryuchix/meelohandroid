package com.meeloh.meeloh.Home.Fragments.Children;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Children.ViewChildActivity;
import com.meeloh.meeloh.Children.AddChildActivity;
import com.meeloh.meeloh.EditRiskFactorQuestionsActivity;
import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonArrayRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.RiskFactorQuestionsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChildrenFragment extends Fragment {

    private static final String TAG = "ChildrenFragment";
    private LinearLayout mChild;
    private FloatingActionButton addFab;
    private ChildrenInterface mCallback;
    private ChildrenAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Object> mListItems;
    private RecyclerView mRecylerView;

    public ChildrenFragment() {
        // Required empty public constructor
    }

    public static ChildrenFragment newInstance(String param1, String param2) {
        ChildrenFragment fragment = new ChildrenFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_children, container, false);

        mListItems = new ArrayList<>();
        mCallback = new ChildrenInterface() {
            @Override
            public void onViewChildren(int position) {
                viewChild(position);
            }
        };

        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ChildrenAdapter(getActivity(), mListItems,
                mCallback);
        mRecylerView = view.findViewById(R.id.fragment_children_rv);
        mRecylerView.setLayoutManager(mLayoutManager);
        mRecylerView.setAdapter(mAdapter);

        return view;

    }

    private void viewChild(int id) {
        Intent intent = new Intent(getActivity(), ViewChildActivity.class);
        intent.putExtra("children_id", id);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        getChildren();
        super.onResume();
        mListItems.clear();
    }

    private void getChildren() {
        new SendRequest(getActivity()).JsonArrayGetRequest(
                getString(R.string.get_children_url),
                new VolleyJsonArrayRequestCallback() {
                    @Override
                    public void onRequestSuccess(JSONArray response) {
                        Log.d("dfgsdf", String.valueOf(response.length()));
                        if (response.length() > 0) {
                            setChildren(response);
                        }
                    }

                    @Override
                    public void onRequestFailed(VolleyError error) {

                    }
                });
    }

    private void setChildren(JSONArray response) {
        try {
            if (response != null && response.length() > 0) {
                for (int index = 0; index < response.length(); index++) {
                    JSONObject value = (JSONObject) response.get(index);
                    Children child = new Children();

                    child.setId(value.getInt("id"));
                    child.setFirstname(value.getString("firstname"));
                    child.setLastname(value.getString("lastname"));
                    child.setBirthday(value.getString("birthday"));
                    child.setImage(value.getString("image"));
                    child.setGender(value.getString("gender"));

                    mListItems.add(child);
                }
            }

            setAdapterData();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapterData() {
        mAdapter.notifyDataSetChanged();
    }
}
