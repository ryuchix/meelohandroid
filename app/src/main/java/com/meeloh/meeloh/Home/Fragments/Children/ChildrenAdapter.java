package com.meeloh.meeloh.Home.Fragments.Children;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChildrenAdapter extends RecyclerView.Adapter<ChildrenAdapter.ChildrenHolder>{
    private Context mContext;
    private List<Object> mListItems;
    private ChildrenInterface mCallback;

    public ChildrenAdapter(Context context, List<Object> items, ChildrenInterface callback) {
        mContext = context;
        mListItems = items;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ChildrenHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_children_list,
                parent, false);
        return new ChildrenHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildrenHolder holder, final int i) {
        Children child = (Children) mListItems.get(i);

        holder.name.setText(child.getFirstname() + " " + child.getLastname());
        if (child.getGender().equals("Male")) {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.male, 0);
        } else {
            holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.female, 0);
        }
        holder.birthday.setText(child.getBirthday());
        Picasso.get().load(child.getImage()).placeholder(R.drawable.user_default).into(holder.image);
        if (child.getImage() != null && !child.getImage().equals("null")) {
            holder.imageOverlay.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return mListItems != null ? mListItems.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ChildrenHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView image;
        TextView name, birthday;
        LinearLayout parentll;
        ImageView imageOverlay;

        public ChildrenHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_text);
            birthday = itemView.findViewById(R.id.birthday_tv);
            image = itemView.findViewById(R.id.profile_image);
            parentll = itemView.findViewById(R.id.parent_ll);
            parentll.setOnClickListener(this);
            imageOverlay = itemView.findViewById(R.id.profile_image_iv);
        }

        @Override
        public void onClick(View v) {
            Children children = (Children) mListItems.get(getAdapterPosition());

            switch (v.getId()) {
                case R.id.parent_ll: {
                    mCallback.onViewChildren(children.getId());
                    break;
                }
            }
        }
    }

}
