package com.meeloh.meeloh.Home.Fragments.Profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorProfileFragment extends Fragment implements View.OnClickListener {
    private JSONObject detailsObject;
    private TextView mName, mPractice, mLicense, mClinicName, mClinicAddress, mPhone, monday, tuesday, wednesday, thursday, friday, saturday, sunday;
    private User mUser;
    private CircleImageView mImage;
    private ImageView cImage;
    private int doctorId;


    public DoctorProfileFragment() {
        // Required empty public constructor
    }

    public static DoctorProfileFragment newInstance(String param1, String param2) {
        DoctorProfileFragment fragment = new DoctorProfileFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_profile, container, false);

        mUser = AccountsManager.getInstance().getUser();

        mName = view.findViewById(R.id.name_tv);
        mPractice = view.findViewById(R.id.practice_tv);
        mLicense = view.findViewById(R.id.license_tv);
        mImage = view.findViewById(R.id.profile_image);
        cImage = view.findViewById(R.id.profile_image_iv);
        mClinicName = view.findViewById(R.id.clinic_name_tv);
        mClinicAddress = view.findViewById(R.id.clinic_address_tv);
        mPhone = view.findViewById(R.id.clinic_phone_tv);
        mPhone.setOnClickListener(this);
        monday = view.findViewById(R.id.monday_sked);
        tuesday = view.findViewById(R.id.tuesday_sked);
        wednesday = view.findViewById(R.id.wednesday_sked);
        thursday = view.findViewById(R.id.thursday_sked);
        friday = view.findViewById(R.id.friday_sked);
        saturday = view.findViewById(R.id.saturday_sked);
        sunday = view.findViewById(R.id.sunday_sked);

        return view;
    }

    private void getProfile() {
        String mUrl = getString(R.string.get_user_details_url) + mUser.getId();
        new SendRequest(getActivity()).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setprofile(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                    }

                });
    }

    @SuppressLint("SetTextI18n")
    private void setprofile(JSONObject response) {
        try {
            JSONObject doctor = response.optJSONObject("doctor");
            mName.setText(response.getString("firstname") + " " + response.getString("lastname"));
            mPractice.setText(doctor.getString("practice"));
            mLicense.setText("License #: " + doctor.getString("license"));
            mClinicName.setText(doctor.getString("clinic_name"));
            mClinicAddress.setText(doctor.getString("clinic_address"));
            mPhone.setText(doctor.getString("phone"));
            Picasso.get().load(doctor.getString("image")).placeholder(R.drawable.user_default).into(mImage);

            if (doctor.getString("image") != null && !doctor.getString("image").equals("null")) {
                cImage.setVisibility(View.INVISIBLE);
            }

            JSONObject schedule = response.optJSONObject("schedule");

            monday.setText(schedule.getInt("monday_close") == 1 ? "Closed" : schedule.getString("monday_start") + " - " + schedule.getString("monday_end"));
            tuesday.setText(schedule.getInt("tuesday_close") == 1 ? "Closed" : schedule.getString("tuesday_start") + " - " + schedule.getString("tuesday_end"));
            wednesday.setText(schedule.getInt("wednesday_close") == 1 ? "Closed" : schedule.getString("wednesday_start") + " - " + schedule.getString("wednesday_end"));
            thursday.setText(schedule.getInt("thursday_close") == 1 ? "Closed" : schedule.getString("thursday_start") + " - " + schedule.getString("thursday_end"));
            friday.setText(schedule.getInt("friday_close") == 1 ? "Closed" : schedule.getString("friday_start") + " - " + schedule.getString("friday_end"));
            saturday.setText(schedule.getInt("saturday_close") == 1 ? "Closed" : schedule.getString("saturday_start") + " - " + schedule.getString("saturday_end"));
            sunday.setText(schedule.getInt("sunday_close") == 1 ? "Closed" : schedule.getString("sunday_start") + " - " + schedule.getString("sunday_end"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        getProfile();
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clinic_phone_tv:
                openDialer(mPhone.getText().toString());
        }
    }

    public void openDialer(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }
}