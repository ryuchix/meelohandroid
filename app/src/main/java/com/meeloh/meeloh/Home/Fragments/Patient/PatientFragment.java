package com.meeloh.meeloh.Home.Fragments.Patient;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Doctor.DoctorList.ViewChild.ViewChildDetailsActivity;
import com.meeloh.meeloh.Doctor.DoctorList.ViewListDoctor;
import com.meeloh.meeloh.Home.Fragments.Children.ChildrenAdapter;
import com.meeloh.meeloh.Home.Fragments.Children.ChildrenInterface;
import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonArrayRequestCallback;
import com.meeloh.meeloh.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PatientFragment extends Fragment {

    private static final String TAG = "PatientFragment";
    private LinearLayout mChild;
    private FloatingActionButton addFab;
    private PatientInterface mCallback;
    private PatientAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Object> mListItems;
    private RecyclerView mRecylerView;

    public PatientFragment() {
        // Required empty public constructor
    }

    public static PatientFragment newInstance(String param1, String param2) {
        PatientFragment fragment = new PatientFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient, container, false);

        mListItems = new ArrayList<>();
        mCallback = new PatientInterface() {
            @Override
            public void onViewPatient(int position, boolean enrolled) {
                viewPatient(position);
            }
        };

        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new PatientAdapter(getActivity(), mListItems,
                mCallback);
        mRecylerView = view.findViewById(R.id.fragment_patient_rv);
        mRecylerView.setLayoutManager(mLayoutManager);
        mRecylerView.setAdapter(mAdapter);

        return view;

    }

    private void viewPatient(int position) {
        Intent intent = new Intent(getActivity(), ViewChildDetailsActivity.class);
        intent.putExtra("children_id", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        mListItems.clear();
        getPatients();
        super.onResume();
    }

    private void getPatients() {
        new SendRequest(getActivity()).JsonArrayGetRequest(
                getString(R.string.get_doctor_petient_url),
                new VolleyJsonArrayRequestCallback() {
                    @Override
                    public void onRequestSuccess(JSONArray response) {
                        Log.d("dfgsdf", String.valueOf(response.length()));
                        if (response.length() > 0) {
                            setPatient(response);
                        }
                    }

                    @Override
                    public void onRequestFailed(VolleyError error) {

                    }
                });
    }

    private void setPatient(JSONArray response) {
        try {
            if (response != null && response.length() > 0) {
                for (int index = 0; index < response.length(); index++) {
                    JSONObject value = (JSONObject) response.get(index);
                    Children child = new Children();

                    child.setId(value.getInt("id"));
                    child.setFirstname(value.getString("firstname"));
                    child.setLastname(value.getString("lastname"));
                    child.setImage(value.getString("image"));
                    child.setChildEnrolled(value.getBoolean("enrolled"));

                    JSONObject parent = value.optJSONObject("parent");
                    JSONObject partner = value.optJSONObject("partner");
                    child.setParentName(parent.getString("firstname") + " & " + partner.getString("firstname") + " " + parent.getString("lastname"));

                    mListItems.add(child);
                }
            }

            setAdapterData();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapterData() {
        mAdapter.notifyDataSetChanged();
    }

}
