package com.meeloh.meeloh.Home.Fragments.Patient;

import android.support.annotation.Nullable;

public interface PatientInterface {
    void onViewPatient(int position, boolean enrolled);
}
