package com.meeloh.meeloh;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EditRiskFactorQuestionsActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mSubmitBtn, mLaterBtn;
    private Toolbar mToolbar;
    private ProgressBar mProgress;
    private RadioGroup mRadioGroup1, mRadioGroup2, mRadioGroup4, mRadioGroup5, mRadioGroup6, mRadioGroup7, mRadioGroup8;
    private RadioButton mRadioButton1, mRadioButton2, mRadioButton4, mRadioButton5, mRadioButton6, mRadioButton7, mRadioButton8;
    int progress;
    private HashMap<String , String > requestBody;
    private User mUser;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_risk_factor_questions);

        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.risk_factor_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mUser = AccountsManager.getInstance().getUser();

        mSubmitBtn = findViewById(R.id.submit_answer_btn);
        mSubmitBtn.setOnClickListener(this);
        mLaterBtn = findViewById(R.id.answer_later_btn);
        mLaterBtn.setOnClickListener(this);
        mProgress = findViewById(R.id.progress_bar);

        mRadioGroup1 = (RadioGroup) findViewById(R.id.rg_1);
        mRadioGroup2 = (RadioGroup) findViewById(R.id.rg_2);
        mRadioGroup4 = (RadioGroup) findViewById(R.id.rg_4);
        mRadioGroup5 = (RadioGroup) findViewById(R.id.rg_5);
        mRadioGroup6 = (RadioGroup) findViewById(R.id.rg_6);
        mRadioGroup7 = (RadioGroup) findViewById(R.id.rg_7);
        mRadioGroup8 = (RadioGroup) findViewById(R.id.rg_8);

        progress = 0;

        userId = getIntent().getIntExtra("userid", 0);

        mRadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }

                mRadioButton1 = (RadioButton) findViewById(checkedId);

            }
        });

        mRadioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton2 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton4 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton5 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 15;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton6 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 15;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton7 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 15;
                    mProgress.setProgress(progress);
                    selected =  false;
                }
                mRadioButton8 = (RadioButton) findViewById(checkedId);
            }
        });
    }

    @Override
    protected void onResume() {
        getAnswer();
        super.onResume();
    }

    private void getAnswer() {
        String mUrl = getString(R.string.view_answer_risk_factor_url) + userId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setAnswer(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setAnswer(JSONObject response) {
        try {
            if (response.getInt("user_id") != mUser.getId()) {
                mSubmitBtn.setVisibility(View.GONE);
                mSubmitBtn.setClickable(false);
                mLaterBtn.setVisibility(View.GONE);
                mLaterBtn.setClickable(false);
            } else {
                mSubmitBtn.setClickable(true);
                mLaterBtn.setClickable(true);
            }

            if (response.getString("q1").equals("Yes")) {
                mRadioGroup1.check(R.id.radioButton1);
            } else {
                if (response.getString("q1") == null || response.getString("q1").equals("null")) {
                    mRadioGroup1.clearCheck();
                } else {
                    mRadioGroup1.check(R.id.radioButton2);
                }
            }
            if (response.getString("q2").equals("Yes")) {
                mRadioGroup2.check(R.id.radioButton3);
            } else {
                if (response.getString("q2") == null || response.getString("q2").equals("null")) {
                    mRadioGroup2.clearCheck();
                } else {
                    mRadioGroup2.check(R.id.radioButton4);
                }
            }
            if (response.getString("q3").equals("Yes")) {
                mRadioGroup4.check(R.id.radioButton7);
            } else {
                if (response.getString("q3") == null || response.getString("q3").equals("null")) {
                    mRadioGroup4.clearCheck();
                } else {
                    mRadioGroup4.check(R.id.radioButton8);
                }
            }
            if (response.getString("q4").equals("Yes")) {
                mRadioGroup5.check(R.id.radioButton9);
            } else {
                if (response.getString("q4") == null || response.getString("q4").equals("null")) {
                    mRadioGroup5.clearCheck();
                } else {
                    mRadioGroup5.check(R.id.radioButton10);
                }
            }
            if (response.getString("q5").equals("Yes")) {
                mRadioGroup6.check(R.id.radioButton11);
            } else {
                if (response.getString("q5") == null || response.getString("q5").equals("null")) {
                    mRadioGroup6.clearCheck();
                } else {
                    mRadioGroup6.check(R.id.radioButton12);
                }
            }
            if (response.getString("q6").equals("Yes")) {
                mRadioGroup7.check(R.id.radioButton13);
            } else {
                if (response.getString("q6") == null || response.getString("q6").equals("null")) {
                    mRadioGroup7.clearCheck();
                } else {
                    mRadioGroup7.check(R.id.radioButton14);
                }
            }
            if (response.getString("q7").equals("Yes")) {
                mRadioGroup8.check(R.id.radioButton15);
            } else {
                if (response.getString("q7") == null || response.getString("q7").equals("null")) {
                    mRadioGroup8.clearCheck();
                } else {
                    mRadioGroup8.check(R.id.radioButton16);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_answer_btn:

                if (mRadioGroup1.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup1.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup2.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup4.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup5.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup6.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup7.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup8.getCheckedRadioButtonId() != -1) {
                        submitAnswer(1);
                } else {
                    Toast.makeText(EditRiskFactorQuestionsActivity.this, "Answer all questions.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.answer_later_btn:
                answerLater(0);
                break;
        }
    }

    private void answerLater(int status) {
        String mUrl = getString(R.string.answer_risk_factor_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("status", String.valueOf(status));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Intent intent = new Intent(EditRiskFactorQuestionsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    private void submitAnswer(int status) {
        String mUrl = getString(R.string.answer_risk_factor_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("q1", mRadioButton1.getText().toString());
        requestBody.put("q2", String.valueOf(mRadioButton2.getText()));
        requestBody.put("q3", String.valueOf(mRadioButton4.getText()));
        requestBody.put("q4", String.valueOf(mRadioButton5.getText()));
        requestBody.put("q5", String.valueOf(mRadioButton6.getText()));
        requestBody.put("q6", String.valueOf(mRadioButton7.getText()));
        requestBody.put("q7", mRadioButton1.getText().toString());
        requestBody.put("status", String.valueOf(status));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(EditRiskFactorQuestionsActivity.this, "Answers submitted.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(EditRiskFactorQuestionsActivity.this, RiskFactorResultsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }
}