package com.meeloh.meeloh.Doctor.DoctorList.Patient;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewAddPatient extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private TextView mName, mBirthday, mParentName, mParentPhone, mParentSex, mParentBlood, mPartnerName, mPartnerPhone, mPartnerSex, mPartnerBlood;
    private CircleImageView mImage, mParentImage, mPartnerImage;
    private ImageView cImage, parentImage, partnerImage;
    private int childId;
    private Button mAdd;
    private HashMap<String , Integer > requestBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_add_patient);

        childId = getIntent().getIntExtra("children_id", 0);
        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Add patient");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mName = findViewById(R.id.name_tv);
        mBirthday = findViewById(R.id.birthday_tv);
        mImage = findViewById(R.id.profile_image);
        cImage = findViewById(R.id.profile_image_iv);

        mParentName = findViewById(R.id.parent_name_text);
        mParentPhone = findViewById(R.id.parent_phone_tv);
        mParentSex = findViewById(R.id.parent_sex);
        mParentBlood = findViewById(R.id.parent_blood);
        mParentImage = findViewById(R.id.parent_image);
        parentImage = findViewById(R.id.parent_image_iv);

        mPartnerName = findViewById(R.id.partner_name_text);
        mPartnerPhone = findViewById(R.id.partner_phone_tv);
        mPartnerSex = findViewById(R.id.partner_sex);
        mPartnerBlood = findViewById(R.id.partner_blood);
        mPartnerImage = findViewById(R.id.partner_image);
        partnerImage = findViewById(R.id.partner_image_iv);
        
        mAdd = findViewById(R.id.add_to_list_patient_btn);
        mAdd.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        getChild();
        super.onResume();
    }

    private void getChild() {
        String mUrl = getString(R.string.get_child_url) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setChild(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    @SuppressLint("SetTextI18n")
    private void setChild(JSONObject response) {
        try {
            mName.setText(response.getString("firstname") + " " + response.getString("lastname"));
            mBirthday.setText(response.getString("birthday"));

            Picasso.get().load(response.getString("image")).placeholder(R.drawable.user_default).into(mImage);
            String gender = response.getString("gender");
            if (gender.equals("Male")) {
                mName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.male, 0);
            } else {
                mName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.female, 0);
            }
            if (response.getString("image") != null && !response.getString("image").equals("null")) {
                cImage.setVisibility(View.INVISIBLE);
            }

            JSONObject parents = response.optJSONObject("parents");
            JSONObject parent = response.optJSONObject("parent");
            JSONObject partner = parents.optJSONObject("partner");

            mParentName.setText(parent.getString("firstname") + " " + parent.getString("lastname"));
            mParentPhone.setText(parents.getString("phone"));
            mParentSex.setText("Sex: " + parents.getString("gender"));
            mParentBlood.setText("Blood type: " + parents.getString("blood_type"));

            mPartnerName.setText(partner.getString("firstname") + " " + partner.getString("lastname"));
            mPartnerPhone.setText(partner.getString("phone"));
            mPartnerSex.setText("Sex: " + partner.getString("gender"));
            mPartnerBlood.setText("Blood type: " + partner.getString("blood_type"));

            Picasso.get().load(partner.getString("image")).placeholder(R.drawable.user_default).into(mParentImage);
            Picasso.get().load(parents.getString("image")).placeholder(R.drawable.user_default).into(mPartnerImage);

            if (partner.getString("image") != null && !partner.getString("image").equals("null")) {
                partnerImage.setVisibility(View.INVISIBLE);
            }

            if (parents.getString("image") != null && !parents.getString("image").equals("null")) {
                parentImage.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_to_list_patient_btn:
                addChild();
                break;
        }
    }

    private void addChild() {
        String mUrl = getString(R.string.add_patient_url);
        requestBody = new HashMap<String, Integer>();
        requestBody.put("child_id", childId);

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(ViewAddPatient.this, "Patient added.", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
