package com.meeloh.meeloh.Doctor.DoctorList;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Doctor.DoctorList.AddDoctor.AddDoctorActivity;
import com.meeloh.meeloh.EditRiskFactorQuestionsActivity;
import com.meeloh.meeloh.Models.Doctor;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonArrayRequestCallback;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.RiskFactorResultsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DoctorListActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private FloatingActionButton mFab;
    private DoctorListInterface mCallback;
    private DoctorListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Object> mListItems;
    private RecyclerView mRecylerView;
    private TextView mEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("List of doctors");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mListItems = new ArrayList<>();
        mCallback = new DoctorListInterface() {
            @Override
            public void onViewDoctor(int position) {
                viewDoctor(position);
            }
        };

        mEmpty = findViewById(R.id.no_result_tv);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new DoctorListAdapter(this, mListItems,
                mCallback);
        mRecylerView = findViewById(R.id.doctor_list_rv);
        mRecylerView.setLayoutManager(mLayoutManager);
        mRecylerView.setAdapter(mAdapter);

        mFab = findViewById(R.id.add_fab);
        mFab.setOnClickListener(this);
    }

    private void viewDoctor(int position) {
        Intent intent = new Intent(this, ViewListDoctor.class);
        intent.putExtra("doctor_id", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        getDoctors();
        super.onResume();
    }

    private void getDoctors() {
        new SendRequest(this).JsonArrayGetRequest(
                getString(R.string.get_parent_doctors_url),
                new VolleyJsonArrayRequestCallback() {
                    @Override
                    public void onRequestSuccess(JSONArray response) {
                        if (response.length() > 0) {
                            mEmpty.setVisibility(View.GONE);
                            setDoctors(response);
                        } else {
                            mEmpty.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onRequestFailed(VolleyError error) {

                    }
                });
    }

    private void setDoctors(JSONArray response) {
        mListItems.clear();
        try {
            if (response != null && response.length() > 0) {
                for (int index = 0; index < response.length(); index++) {
                    JSONObject value = (JSONObject) response.get(index);
                    Doctor doctor = new Doctor();

                    JSONObject user = value.optJSONObject("user");
                    doctor.setId(value.getInt("id"));
                    doctor.setFirstname(user.getString("firstname"));
                    doctor.setLastname(user.getString("lastname"));

                    doctor.setPractice(value.getString("practice"));
                    doctor.setImage(value.getString("image"));

                    mListItems.add(doctor);
                }
            }

            setAdapterData();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapterData() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_fab:
                addDoctor();
                break;
        }
    }

    private void addDoctor() {
        Intent intent = new Intent(this, AddDoctorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
