package com.meeloh.meeloh.Doctor.DoctorList.ViewChild;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Children.BirthDetailsActivity;
import com.meeloh.meeloh.Children.EditChildActivity;
import com.meeloh.meeloh.Children.ImmunisationRecordsActivity;
import com.meeloh.meeloh.Children.PeriodicCheckActivity;
import com.meeloh.meeloh.Doctor.DoctorList.DoctorListActivity;
import com.meeloh.meeloh.Doctor.DoctorList.ViewListDoctor;
import com.meeloh.meeloh.MainActivity;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.Patient.BirthDetails.PatientBirthDetailsActivity;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewChildDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private LinearLayout parents, birthDetails, immunisationRecords, periodicChecks, illnessRecords, treatmentRecords;
    private TextView mName, mBirthday;
    private CircleImageView mImage;
    private ImageView cImage;
    private int childId, doctorId;
    private HashMap<String , String > requestBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_child_details);

        childId = getIntent().getIntExtra("children_id", 0);
        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Patients");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        parents = findViewById(R.id.parents_ll);
        parents.setOnClickListener(this);
        birthDetails = findViewById(R.id.birthdetails_ll);
        birthDetails.setOnClickListener(this);
        immunisationRecords = findViewById(R.id.immu_ll);
        immunisationRecords.setOnClickListener(this);
        periodicChecks = findViewById(R.id.periodiccheck_ll);
        periodicChecks.setOnClickListener(this);
        illnessRecords = findViewById(R.id.illnessrecords_ll);
        illnessRecords.setOnClickListener(this);
        treatmentRecords = findViewById(R.id.treatmentrecords_ll);
        treatmentRecords.setOnClickListener(this);

        mName = findViewById(R.id.name_tv);
        mBirthday = findViewById(R.id.birthday_tv);
        mImage = findViewById(R.id.profile_image);
        cImage = findViewById(R.id.profile_image_iv);
    }

    @Override
    protected void onResume() {
        getChild();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void getChild() {
        String mUrl = getString(R.string.get_child_url) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setChild(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    @SuppressLint("SetTextI18n")
    private void setChild(JSONObject response) {
        try {
            mName.setText(response.getString("firstname") + " " + response.getString("lastname"));
            mBirthday.setText(response.getString("birthday"));

            Picasso.get().load(response.getString("image")).placeholder(R.drawable.user_default).into(mImage);
            String gender = response.getString("gender");
            if (gender.equals("Male")) {
                mName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.male, 0);
            } else {
                mName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.female, 0);
            }
            if (response.getString("image") != null && !response.getString("image").equals("null")) {
                cImage.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.children_menu:
                openActivity();
                break;
            case R.id.birthdetails_ll:
                openBirthDetails();
                break;
            case R.id.immu_ll:
                openImmunisation();
                break;
            case R.id.periodiccheck_ll:
                openPeriodicCheck();
                break;
            case R.id.illnessrecords_ll:
                openIllnessRecords();
                break;
            case R.id.treatmentrecords_ll:
                openTreatmentRecords();
                break;
            case R.id.parents_ll:
                openParents();
                break;
        }
    }

    private void openParents() {
    }

    private void openTreatmentRecords() {
        Toast.makeText(this, "Treatment records", Toast.LENGTH_SHORT).show();
    }

    private void openIllnessRecords() {
        Toast.makeText(this, "Illness records", Toast.LENGTH_SHORT).show();
    }

    private void openPeriodicCheck() {
        Intent intent;
        intent = new Intent(this, PeriodicCheckActivity.class);
        startActivity(intent);
    }

    private void openImmunisation() {
        Intent intent;
        intent = new Intent(this, ImmunisationRecordsActivity.class);
        startActivity(intent);
    }

    private void openBirthDetails() {
        Intent intent;
        intent = new Intent(this, PatientBirthDetailsActivity.class);
        intent.putExtra("children_id", childId);
        startActivity(intent);
    }

    private void openActivity() {
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 1);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dots, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.remove_from_list) {
            openDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openDialog() {

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Remove patient");
        mBuilder.setMessage("Are you sure you want to remove " + mName.getText() + "?" );
        mBuilder.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removePatient();
            }
        });
        mBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = mBuilder.create();
        dialog.show();
    }

    private void removePatient() {
        String mUrl = getString(R.string.remove_patient_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("child_id", String.valueOf(childId));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(ViewChildDetailsActivity.this, "Patient removed.", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }
}
