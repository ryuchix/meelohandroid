package com.meeloh.meeloh.Doctor.DoctorList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.meeloh.meeloh.Models.Doctor;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.DoctorHolder>{
    private Context mContext;
    private List<Object> mListItems;
    private DoctorListInterface mCallback;

    public DoctorListAdapter(Context context, List<Object> items, DoctorListInterface callback) {
        mContext = context;
        mListItems = items;
        mCallback = callback;
    }

    @NonNull
    @Override
    public DoctorListAdapter.DoctorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doctor_list,
                parent, false);
        return new DoctorListAdapter.DoctorHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DoctorListAdapter.DoctorHolder holder, final int i) {
        Doctor doctor = (Doctor) mListItems.get(i);

        holder.name.setText(doctor.getFirstname() + " " + doctor.getLastname());
        holder.practice.setText(doctor.getPractice());
        Picasso.get().load(doctor.getImage()).placeholder(R.drawable.user_default).into(holder.image);
        if (doctor.getImage() != null && !doctor.getImage().equals("null")) {
            holder.imageOverlay.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class DoctorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView image;
        TextView name, practice;
        LinearLayout parentll;
        ImageView imageOverlay;

        public DoctorHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_text);
            image = itemView.findViewById(R.id.profile_image);
            parentll = itemView.findViewById(R.id.parent_ll);
            parentll.setOnClickListener(this);
            imageOverlay = itemView.findViewById(R.id.profile_image_iv);
            practice = itemView.findViewById(R.id.practice_tv);
        }

        @Override
        public void onClick(View v) {
            Doctor doctor = (Doctor) mListItems.get(getAdapterPosition());

            switch (v.getId()) {
                case R.id.parent_ll: {
                    mCallback.onViewDoctor(doctor.getId());
                    break;
                }
            }
        }
    }

}