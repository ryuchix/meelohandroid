package com.meeloh.meeloh.Doctor.DoctorList.Patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Doctor.DoctorList.ViewChild.ViewChildDetailsActivity;
import com.meeloh.meeloh.Home.Fragments.Patient.PatientAdapter;
import com.meeloh.meeloh.Home.Fragments.Patient.PatientInterface;
import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonArrayRequestCallback;
import com.meeloh.meeloh.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddPatientActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private PatientInterface mCallback;
    private PatientAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Object> mListItems;
    private RecyclerView mRecylerView;
    private TextInputEditText mSearch;
    private TextView mEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Add patient");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mListItems = new ArrayList<>();
        mCallback = new PatientInterface() {
            @Override
            public void onViewPatient(int position, boolean enrolled) {
                viewPatient(position, enrolled);
            }
        };
        mEmpty = findViewById(R.id.no_result_tv);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new PatientAdapter(this, mListItems,
                mCallback);
        mRecylerView = findViewById(R.id.search_patient_rv);
        mRecylerView.setLayoutManager(mLayoutManager);
        mRecylerView.setAdapter(mAdapter);

        mSearch = findViewById(R.id.search_tiet);
        mSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                String editableString = s.toString();
                if (editableString.trim().length() != 0) {
                    final String spaceFreeString = editableString.replaceAll(" ", "");
                    getSearch(editableString);
                } else {
                    mListItems.clear();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

    }

    private void viewPatient(int position, boolean enrolled) {
        if (enrolled) {
            Intent intent = new Intent(this, ViewChildDetailsActivity.class);
            intent.putExtra("children_id", position);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ViewAddPatient.class);
            intent.putExtra("children_id", position);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    private void getSearch(String search) {

        new SendRequest(this).JsonArrayGetRequest(
                getString(R.string.search_patients_url) + search,
                new VolleyJsonArrayRequestCallback() {
                    @Override
                    public void onRequestSuccess(JSONArray response) {
                        if (response.length() > 0) {
                            mEmpty.setVisibility(View.GONE);
                            setSearch(response);
                        } else {
                            mEmpty.setVisibility(View.VISIBLE);
                            setAdapterData();
                        }
                    }

                    @Override
                    public void onRequestFailed(VolleyError error) {

                    }
                });
    }

    private void setSearch(JSONArray response) {
        mListItems.clear();
        try {
            if (response != null && response.length() > 0) {
                for (int index = 0; index < response.length(); index++) {
                    JSONObject value = (JSONObject) response.get(index);
                    Children child = new Children();

                    JSONObject partner = value.optJSONObject("partner");
                    JSONObject parent = value.optJSONObject("parent");

                    child.setId(value.getInt("id"));
                    child.setFirstname(value.getString("firstname"));
                    child.setLastname(value.getString("lastname"));
                    child.setImage(value.getString("image"));
                    child.setChildEnrolled(value.getBoolean("enrolled"));

                    child.setParentName(parent.getString("firstname") + " & " + partner.getString("firstname") + " " + parent.getString("lastname"));

                    mListItems.add(child);
                }
            }

            setAdapterData();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapterData() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        mListItems.clear();
        super.onResume();
    }
}
