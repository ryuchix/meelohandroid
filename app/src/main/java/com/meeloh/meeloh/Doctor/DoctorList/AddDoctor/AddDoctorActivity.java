package com.meeloh.meeloh.Doctor.DoctorList.AddDoctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Doctor.DoctorList.DoctorListAdapter;
import com.meeloh.meeloh.Doctor.DoctorList.DoctorListInterface;
import com.meeloh.meeloh.Doctor.DoctorList.ViewAddDoctor.ViewAddDoctor;
import com.meeloh.meeloh.Models.Doctor;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonArrayRequestCallback;
import com.meeloh.meeloh.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddDoctorActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private DoctorListInterface mCallback;
    private DoctorListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Object> mListItems;
    private RecyclerView mRecylerView;
    private TextInputEditText mSearch;
    private TextView mEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Add doctor");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mListItems = new ArrayList<>();
        mCallback = new DoctorListInterface() {
            @Override
            public void onViewDoctor(int position) {
                viewDoctor(position);
            }
        };
        mEmpty = findViewById(R.id.no_result_tv);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new DoctorListAdapter(this, mListItems,
                mCallback);
        mRecylerView = findViewById(R.id.search_doctor_rv);
        mRecylerView.setLayoutManager(mLayoutManager);
        mRecylerView.setAdapter(mAdapter);

        mSearch = findViewById(R.id.search_tiet);
        mSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                String editableString = s.toString();
                if (editableString.trim().length() != 0) {
                    final String spaceFreeString = editableString.replaceAll(" ", "");
                    getSearch(editableString);
                } else {
                    mListItems.clear();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

    }

    private void viewDoctor(int position) {
        Intent intent = new Intent(this, ViewAddDoctor.class);
        intent.putExtra("doctor_id", position);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void getSearch(String search) {

        new SendRequest(this).JsonArrayGetRequest(
                getString(R.string.search_doctors_url) + search,
                new VolleyJsonArrayRequestCallback() {
                    @Override
                    public void onRequestSuccess(JSONArray response) {
                        if (response.length() > 0) {
                            mEmpty.setVisibility(View.GONE);
                            setSearch(response);
                        } else {
                            mEmpty.setVisibility(View.VISIBLE);
                            setAdapterData();
                        }
                    }

                    @Override
                    public void onRequestFailed(VolleyError error) {

                    }
                });
    }

    private void setSearch(JSONArray response) {
        mListItems.clear();
        try {
            if (response != null && response.length() > 0) {
                for (int index = 0; index < response.length(); index++) {
                    JSONObject value = (JSONObject) response.get(index);
                    Doctor doctor = new Doctor();

                    JSONObject user = value.optJSONObject("user");
                    doctor.setId(value.getInt("id"));
                    doctor.setFirstname(user.getString("firstname"));
                    doctor.setLastname(user.getString("lastname"));

                    doctor.setPractice(value.getString("practice"));
                    doctor.setImage(value.getString("image"));

                    mListItems.add(doctor);
                }
            }

            setAdapterData();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapterData() {
        mAdapter.notifyDataSetChanged();
    }

}
