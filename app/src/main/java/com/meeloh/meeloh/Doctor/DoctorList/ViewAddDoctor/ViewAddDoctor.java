package com.meeloh.meeloh.Doctor.DoctorList.ViewAddDoctor;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Children.ViewChildActivity;
import com.meeloh.meeloh.Doctor.DoctorList.DoctorListInterface;
import com.meeloh.meeloh.Doctor.DoctorList.ViewListDoctor;
import com.meeloh.meeloh.Doctor.DoctorList.ViewListDoctorAdapter;
import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonArrayRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewAddDoctor extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private TextView mName, mPractice, mLicense, mClinicName, mClinicAddress, mPhone, monday, tuesday, wednesday, thursday, friday, saturday, sunday;
    private CircleImageView mImage;
    private ImageView cImage;
    private int doctorId;
    private Button mAddToListDoctor;
    private CheckBox mAssignFamilyDoctor;
    private int checkValue = 0;
    private HashMap<String , Integer > requestBody;
    private LinearLayout edit, delete;
    private BottomSheetDialog mBottomSheetDialog;
    private View sheetView;
    private ViewAddDoctorAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Object> mListItems;
    private RecyclerView mRecylerView;
    private ViewAddDoctorInterface mCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_add_doctor);

        doctorId = getIntent().getIntExtra("doctor_id", 0);
        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Add doctor");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mListItems = new ArrayList<>();

        mCallback = new ViewAddDoctorInterface() {
            @Override
            public void onViewChild(int position) {
                viewChild(position);
            }
        };
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ViewAddDoctorAdapter(this, mListItems, mCallback);

        mBottomSheetDialog = new BottomSheetDialog(this);
        sheetView = this.getLayoutInflater().inflate(R.layout.bottom_sample, null);

        mRecylerView = sheetView.findViewById(R.id.child_list_rv);
        mRecylerView.setLayoutManager(mLayoutManager);
        mRecylerView.setAdapter(mAdapter);

        mBottomSheetDialog.setContentView(sheetView);

        mName = findViewById(R.id.name_tv);
        mPractice = findViewById(R.id.practice_tv);
        mLicense = findViewById(R.id.license_tv);
        mImage = findViewById(R.id.profile_image);
        cImage = findViewById(R.id.profile_image_iv);
        mClinicName = findViewById(R.id.clinic_name_tv);
        mClinicAddress = findViewById(R.id.clinic_address_tv);
        mPhone = findViewById(R.id.clinic_phone_tv);
        mPhone.setOnClickListener(this);
        monday = findViewById(R.id.monday_sked);
        tuesday = findViewById(R.id.tuesday_sked);
        wednesday = findViewById(R.id.wednesday_sked);
        thursday = findViewById(R.id.thursday_sked);
        friday = findViewById(R.id.friday_sked);
        saturday = findViewById(R.id.saturday_sked);
        sunday = findViewById(R.id.sunday_sked);

        mAddToListDoctor = findViewById(R.id.add_to_list_doctor_btn);
        mAddToListDoctor.setOnClickListener(this);
        mAssignFamilyDoctor = findViewById(R.id.assign_as_family_doctor_checkbox);
    }

    private void viewChild(int position) {
        String mUrl = getString(R.string.add_doctor_url);
        requestBody = new HashMap<String, Integer>();
        requestBody.put("family_doctor", mAssignFamilyDoctor.isChecked() ? 1 : 0);
        requestBody.put("doctor_id", doctorId);
        requestBody.put("child_id", position);

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(ViewAddDoctor.this, "Doctor added.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ViewAddDoctor.this, ViewListDoctor.class);
                        intent.putExtra("doctor_id", doctorId);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    @Override
    protected void onResume() {
        getDoctor();
        getChildren();
        super.onResume();
    }

    private void getChildren() {
        new SendRequest(this).JsonArrayGetRequest(
                getString(R.string.get_children_url),
                new VolleyJsonArrayRequestCallback() {
                    @Override
                    public void onRequestSuccess(JSONArray response) {
                        if (response.length() > 0) {
                            setChildren(response);
                        }
                    }

                    @Override
                    public void onRequestFailed(VolleyError error) {

                    }
                });
    }




    private void setChildren(JSONArray response) {
        try {
            if (response != null && response.length() > 0) {
                for (int index = 0; index < response.length(); index++) {
                    JSONObject value = (JSONObject) response.get(index);
                    Children child = new Children();

                    child.setId(value.getInt("id"));
                    child.setFirstname(value.getString("firstname"));
                    child.setLastname(value.getString("lastname"));

                    mListItems.add(child);
                }
            }

            setAdapterData();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapterData() {
        mAdapter.notifyDataSetChanged();
    }

    private void getDoctor() {
        String mUrl = getString(R.string.get_doctor_url) + doctorId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setDoctor(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    @SuppressLint("SetTextI18n")
    private void setDoctor(JSONObject response) {
        try {
            JSONObject user = response.optJSONObject("user");
            mName.setText(user.getString("firstname") + " " + user.getString("lastname"));
            mPractice.setText(response.getString("practice"));
            mLicense.setText("License #: " + response.getString("license"));
            mClinicName.setText(response.getString("clinic_name"));
            mClinicAddress.setText(response.getString("clinic_address"));
            mPhone.setText(response.getString("phone"));
            Picasso.get().load(response.getString("image")).placeholder(R.drawable.user_default).into(mImage);

            if (response.getString("image") != null && !response.getString("image").equals("null")) {
                cImage.setVisibility(View.INVISIBLE);
            }

            JSONObject schedule = response.optJSONObject("schedule");

            if (schedule != null) {
                monday.setText(schedule.getInt("monday_close") == 1 ? "Closed" : schedule.getString("monday_start") + " - " + schedule.getString("monday_end"));
                tuesday.setText(schedule.getInt("tuesday_close") == 1 ? "Closed" : schedule.getString("tuesday_start") + " - " + schedule.getString("tuesday_end"));
                wednesday.setText(schedule.getInt("wednesday_close") == 1 ? "Closed" : schedule.getString("wednesday_start") + " - " + schedule.getString("wednesday_end"));
                thursday.setText(schedule.getInt("thursday_close") == 1 ? "Closed" : schedule.getString("thursday_start") + " - " + schedule.getString("thursday_end"));
                friday.setText(schedule.getInt("friday_close") == 1 ? "Closed" : schedule.getString("friday_start") + " - " + schedule.getString("friday_end"));
                saturday.setText(schedule.getInt("saturday_close") == 1 ? "Closed" : schedule.getString("saturday_start") + " - " + schedule.getString("saturday_end"));
                sunday.setText(schedule.getInt("sunday_close") == 1 ? "Closed" : schedule.getString("sunday_start") + " - " + schedule.getString("sunday_end"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_to_list_doctor_btn:
                addToList();
                break;
            case R.id.clinic_phone_tv:
                openDialer(mPhone.getText().toString());
        }
    }

    private void addToList() {
        mBottomSheetDialog.show();
    }

    private void openDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.select_child_layout);
        dialog.setTitle("Hello");
        dialog.show();
    }

    public void openDialer(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }
}
