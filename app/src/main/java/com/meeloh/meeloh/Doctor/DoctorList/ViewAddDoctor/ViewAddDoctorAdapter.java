package com.meeloh.meeloh.Doctor.DoctorList.ViewAddDoctor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.R;

import java.util.List;

public class ViewAddDoctorAdapter  extends RecyclerView.Adapter<ViewAddDoctorAdapter.DoctorHolder> {
    private Context mContext;
    private List<Object> mListItems;
    private ViewAddDoctorInterface mCallback;

    public ViewAddDoctorAdapter(Context context, List<Object> items, ViewAddDoctorInterface callback) {
        mContext = context;
        mListItems = items;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewAddDoctorAdapter.DoctorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child_list_bottom,
                parent, false);
        return new ViewAddDoctorAdapter.DoctorHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewAddDoctorAdapter.DoctorHolder holder, final int i) {
        Children child = (Children) mListItems.get(i);

        holder.name.setText(child.getFirstname() + " " + child.getLastname());
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class DoctorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;

        public DoctorHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.child_name_tv);
            name.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Children children = (Children) mListItems.get(getAdapterPosition());

            switch (v.getId()) {
                case R.id.child_name_tv: {
                    mCallback.onViewChild(children.getId());
                    break;
                }
            }
        }
    }

}
