package com.meeloh.meeloh.Doctor.DoctorList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meeloh.meeloh.Models.Children;
import com.meeloh.meeloh.Models.Doctor;
import com.meeloh.meeloh.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewListDoctorAdapter  extends RecyclerView.Adapter<ViewListDoctorAdapter.DoctorHolder>{
    private Context mContext;
    private List<Object> mListItems;

    public ViewListDoctorAdapter(Context context, List<Object> items) {
        mContext = context;
        mListItems = items;
    }

    @NonNull
    @Override
    public ViewListDoctorAdapter.DoctorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child_list_name,
                parent, false);
        return new ViewListDoctorAdapter.DoctorHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewListDoctorAdapter.DoctorHolder holder, final int i) {
        Children child = (Children) mListItems.get(i);

        holder.name.setText(child.getFirstname() + " " + child.getLastname());
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class DoctorHolder extends RecyclerView.ViewHolder {

        TextView name;

        public DoctorHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.child_name_tv);
        }
    }

}