package com.meeloh.meeloh;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.CreateAccount.CreateAccountEmailActivity;
import com.meeloh.meeloh.Login.LoginActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;

import org.json.JSONObject;

public class LandingActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mCreateAccountBtn, mLoginBtn;

    private static final String PASSPORT_TOKEN_KEY = "API_PASSPORT_TOKEN";
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        if (mSharedPreferences.contains(PASSPORT_TOKEN_KEY)) {
            getUser();
        }

        mCreateAccountBtn = findViewById(R.id.create_account_btn);
        mCreateAccountBtn.setOnClickListener(this);
        mLoginBtn = findViewById(R.id.login_btn);
        mLoginBtn.setOnClickListener(this);
    }

    private void getUser() {
                new SendRequest(this).JsonGetRequest(getString(R.string.get_user_url),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setUser(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    private void setUser(JSONObject response) {
        User user = new UserAccountSetter(response).setupUserAccount();

        AccountsManager manager = AccountsManager.getInstance();
        manager.setUser(user);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startIntent(intent);
    }

    private void startIntent(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_btn:
                openCreateAccount();
                break;
            case R.id.login_btn:
                openLogin();
                break;
        }
    }

    private void openLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void openCreateAccount() {
        Intent intent = new Intent(this, CreateAccountEmailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
