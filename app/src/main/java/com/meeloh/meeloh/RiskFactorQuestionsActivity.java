package com.meeloh.meeloh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Children.AddChildActivity;
import com.meeloh.meeloh.CreateAccount.CreateAccountEmailActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.util.HashMap;

public class RiskFactorQuestionsActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mSubmitBtn, mLaterBtn;
    private Toolbar mToolbar;
    private ProgressBar mProgress;
    private RadioGroup mRadioGroup1, mRadioGroup2, mRadioGroup3, mRadioGroup4, mRadioGroup5, mRadioGroup6, mRadioGroup7, mRadioGroup8;
    private RadioButton mRadioButton1, mRadioButton2, mRadioButton4, mRadioButton5, mRadioButton6, mRadioButton7, mRadioButton8;
    int progress;
    private HashMap<String , String > requestBody;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_risk_factor_questions);

        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.risk_factor_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mUser = AccountsManager.getInstance().getUser();

        mSubmitBtn = findViewById(R.id.submit_answer_btn);
        mSubmitBtn.setOnClickListener(this);
        mLaterBtn = findViewById(R.id.answer_later_btn);
        mLaterBtn.setOnClickListener(this);
        mProgress = findViewById(R.id.progress_bar);

        mRadioGroup1 = (RadioGroup) findViewById(R.id.rg_1);
        mRadioGroup2 = (RadioGroup) findViewById(R.id.rg_2);
        mRadioGroup4 = (RadioGroup) findViewById(R.id.rg_4);
        mRadioGroup5 = (RadioGroup) findViewById(R.id.rg_5);
        mRadioGroup6 = (RadioGroup) findViewById(R.id.rg_6);
        mRadioGroup7 = (RadioGroup) findViewById(R.id.rg_7);
        mRadioGroup8 = (RadioGroup) findViewById(R.id.rg_8);

        progress = 14;

        mRadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }

                mRadioButton1 = (RadioButton) findViewById(checkedId);

            }
        });

        mRadioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton2 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton4 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 14;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton5 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 15;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton6 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 15;
                    mProgress.setProgress(progress);
                    selected = false;
                }
                mRadioButton7 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            boolean selected = true;
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (selected) {
                    progress = progress + 15;
                    mProgress.setProgress(progress);
                    selected =  false;
                }
                mRadioButton8 = (RadioButton) findViewById(checkedId);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_answer_btn:
                if (mRadioGroup1.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup1.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup2.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup4.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup5.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup6.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup7.getCheckedRadioButtonId() != -1 &&
                        mRadioGroup8.getCheckedRadioButtonId() != -1) {
                    submitAnswer(1);
                } else {
                    Toast.makeText(RiskFactorQuestionsActivity.this, "Answer all questions.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.answer_later_btn:
                answerLater(0);
                break;
        }
    }

    private void answerLater(int status) {
        Log.d("aaaaa", "answer_later_btn");
        String mUrl = getString(R.string.answer_risk_factor_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("status", String.valueOf(status));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Intent intent = new Intent(RiskFactorQuestionsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    private void submitAnswer(int status) {
        String mUrl = getString(R.string.answer_risk_factor_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("q1", mRadioButton1.getText().toString());
        requestBody.put("q2", String.valueOf(mRadioButton2.getText()));
        requestBody.put("q3", String.valueOf(mRadioButton4.getText()));
        requestBody.put("q4", String.valueOf(mRadioButton5.getText()));
        requestBody.put("q5", String.valueOf(mRadioButton6.getText()));
        requestBody.put("q6", String.valueOf(mRadioButton7.getText()));
        requestBody.put("q7", mRadioButton1.getText().toString());
        requestBody.put("status", String.valueOf(status));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(RiskFactorQuestionsActivity.this, "Answers submitted.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RiskFactorQuestionsActivity.this, RiskFactorResultsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });



    }
}
