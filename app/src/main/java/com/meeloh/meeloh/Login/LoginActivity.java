package com.meeloh.meeloh.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.Dialogs.InformationAlertDialog;
import com.meeloh.meeloh.MainActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleySingleton;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private static final String PASSPORT_TOKEN_KEY = "API_PASSPORT_TOKEN";
    private static final String USER_ID_KEY = "USER_ID";
    private Toolbar mToolbar;
    private RequestQueue mQueue;
    private Button mLogin;
    private TextInputEditText mEmail, mPassword;
    private String email, password;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        mToolbar = findViewById(R.id.login_toolbar);
        mToolbar.setTitle(getString(R.string.login_text));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mLogin = findViewById(R.id.login_btn);
        mLogin.setOnClickListener(this);
        mEmail = findViewById(R.id.email_address_tiet);
        mPassword = findViewById(R.id.password_c_tiet);

        mPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getX() >= (mPassword.getRight() - mPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mPassword.setTransformationMethod(null);
                        return true;
                    }
                } else {
                    mPassword.setTransformationMethod(new PasswordTransformationMethod());

                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                loginUser();
                break;
        }
    }

    private void loginUser() {
        email = mEmail.getText().toString();
        password = mPassword.getText().toString();

        if(email.trim().length() == 0) {
            Toast.makeText(this, "Enter email address", Toast.LENGTH_SHORT).show();
            mEmail.requestFocus();
        } else if (mPassword.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
            mPassword.requestFocus();
        } else {

            Map<String, String> requestBody = new HashMap<String, String>();
            requestBody.put("email", email);
            requestBody.put("password", password);

            new SendRequest(this).JsonPostRequestNoAuth(getString(R.string.login_url),
                    new JSONObject(requestBody),
                    new VolleyJsonPostRequestCallback() {
                        @Override
                        public void onJsonPostRequestSuccess(JSONObject response) {
                            try {
                                handleLoginResponse(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onJsonPostRequestError(VolleyError error) {
                            Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void handleLoginResponse(JSONObject response) throws JSONException {
        User user = new UserAccountSetter(response).setupUserAccount();
        AccountsManager.getInstance().setUser(user);

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.putString(PASSPORT_TOKEN_KEY,
                response.getString("token"));
        mSharedPreferencesEditor.putString(USER_ID_KEY,
                response.getString("id"));
        mSharedPreferencesEditor.commit();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();
    }


}
