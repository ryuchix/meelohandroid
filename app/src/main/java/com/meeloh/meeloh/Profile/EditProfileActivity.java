package com.meeloh.meeloh.Profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.androidnetworking.error.ANError;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.CreateAccount.CreateAccountPartnerActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleyMultipartRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleySingleton;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;

    private String TAG = "EditProfileActivity";
    private LayoutInflater mInflator;
    private Spinner typeSpinner, typeSpinnerPartner;
    private Boolean selected;

    private Button saveBtn;
    private User mUser;
    private TextInputEditText mFirstname, mLastname, mPhone, mGender, mBlood, mPFirstname, mPLastname, mPPhone, mPGender, mPBlood;
    private String fname, lname, phone, blood, pfname, plname, pphone, pblood;
    private HashMap<String , String > requestBody;
    private ImageButton mUploadImage, mPartnerUploadImage;

    private static int REQUEST_READ_STORAGE = 100;
    private int mIsImagePicked = 0;
    private CircleImageView mImage;
    private String mImagePath;
    private int mIsPartnerImagePicked = 0;
    private CircleImageView mPartnerImage;
    private String mPartnerImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        mUser = AccountsManager.getInstance().getUser();
        requestBody = new HashMap<>();

        mToolbar = findViewById(R.id.edit_profile_toolbar);
        mToolbar.setTitle("Edit profile");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        selected = false;

        typeSpinner = (Spinner) findViewById(R.id.spinner);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(typeSelectedListener);
        typeSpinner.setOnTouchListener(typeSpinnerTouchListener);

        typeSpinnerPartner = (Spinner) findViewById(R.id.partner_spinner);
        typeSpinnerPartner.setAdapter(typeSpinnerAdapterPartner);
        typeSpinnerPartner.setOnItemSelectedListener(typeSelectedListenerPartner);
        typeSpinnerPartner.setOnTouchListener(typeSpinnerTouchListenerPartner);

        mInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        saveBtn = findViewById(R.id.save_profile_btn);
        saveBtn.setOnClickListener(this);

        mFirstname = findViewById(R.id.first_name_tiet);
        mLastname = findViewById(R.id.last_name_tiet);
        mPhone = findViewById(R.id.phone_tiet);
        mBlood = findViewById(R.id.blood_type_tiet);

        mPFirstname = findViewById(R.id.partner_first_name_tiet);
        mPLastname = findViewById(R.id.partner_last_name_tiet);
        mPPhone = findViewById(R.id.partner_phone_tiet);
        mPBlood = findViewById(R.id.partner_blood_type_tiet);

        mUploadImage = findViewById(R.id.edit_profile_btn);
        mUploadImage.setOnClickListener(this);
        mPartnerUploadImage = findViewById(R.id.partner_edit_profile_btn);
        mPartnerUploadImage.setOnClickListener(this);

        mImage = findViewById(R.id.profile_image);
        mPartnerImage = findViewById(R.id.partner_profile_image);

        getProfile();
    }

    private void getProfile() {
        String mUrl = getString(R.string.get_user_details_url) + mUser.getId();
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setprofile(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    @SuppressLint("SetTextI18n")
    private void setprofile(JSONObject response) {
        try {
            mFirstname.setText(response.getString("firstname"));
            mLastname.setText(response.getString("lastname"));
            mPhone.setText(response.getString("phone"));
            mBlood.setText(response.getString("blood_type"));
            Picasso.get().load(response.getString("image")).into(mImage);
            String gender = response.getString("gender");

            if (gender.equals("Male")) {
                typeSpinner.setSelection(0);
            } else {
                typeSpinner.setSelection(1);
            }

            JSONObject partner = response.optJSONObject("partner");
            mPFirstname.setText(partner.getString("firstname"));
            mPLastname.setText(partner.getString("lastname"));
            mPPhone.setText(partner.getString("phone"));
            mPBlood.setText(partner.getString("blood_type"));
            Picasso.get().load(partner.getString("image")).into(mPartnerImage);

            String gender2 = partner.getString("gender");

            if (gender2.equals("Male")) {
                typeSpinnerPartner.setSelection(0);
            } else {
                typeSpinnerPartner.setSelection(1);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private SpinnerAdapter typeSpinnerAdapter = new BaseAdapter() {
        private TextView text;
        private String[] data = { "Male", "Female" };
        private int count = 2;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);
            text.setText(data[position]);
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return count;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            Log.d(TAG, "user selected : "
                    + typeSpinner.getSelectedItem().toString());

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            return false;
        }
    };


    private SpinnerAdapter typeSpinnerAdapterPartner = new BaseAdapter() {

        private TextView text;
        private String[] data = { "Male", "Female" };
        private int count = 2;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);
            text.setText(data[position]);
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return count;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListenerPartner = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            Log.d(TAG, "user selected : "
                    + typeSpinner.getSelectedItem().toString());

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListenerPartner = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            ((BaseAdapter) typeSpinnerAdapterPartner).notifyDataSetChanged();
            return false;
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_profile_btn:
                saveProfile();
                break;
            case R.id.edit_profile_btn:
                mIsImagePicked = 1;
                openGallery();
                break;
            case R.id.partner_edit_profile_btn:
                mIsPartnerImagePicked = 1;
                openGallery();
                break;
        }
    }

    private void openGallery() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_READ_STORAGE);
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_READ_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_READ_STORAGE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_READ_STORAGE) {
            if (resultCode == RESULT_OK && data.getData() != null) {
                String imagePath;

                Uri selectedImage = data.getData();
                String[] filePathToColumn = new String[]{MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage, filePathToColumn,
                        null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathToColumn[0]);
                imagePath = cursor.getString(columnIndex);
                cursor.close();

                if (mIsImagePicked == 1) {
                    mImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                    mImagePath = imagePath;
                    mIsImagePicked = 0;
                    openUploadImage();
                }

                if (mIsPartnerImagePicked == 1) {
                    mPartnerImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                    mPartnerImagePath = imagePath;
                    mIsPartnerImagePicked = 0;
                    openPartnerUploadImage();
                }
            }
        }
    }

    private void openPartnerUploadImage() {
        new SendRequest(this).MultipartRequest(getString(R.string.upload_partnerimage_url),
                "partner_image",
                new File(mPartnerImagePath),
                new VolleyMultipartRequestCallback() {
                    @Override
                    public void onNetworSuccessResponse(JSONObject response) {
                        mIsPartnerImagePicked = 2;
                    }

                    @Override
                    public void onNetworErrorResponse(ANError error) {

                    }
                });
    }

    private void openUploadImage() {

        new SendRequest(this).MultipartRequest(getString(R.string.upload_image_url),
                "image",
                new File(mImagePath),
                new VolleyMultipartRequestCallback() {
                    @Override
                    public void onNetworSuccessResponse(JSONObject response) {
                        mIsImagePicked = 2;
                    }

                    @Override
                    public void onNetworErrorResponse(ANError error) {

                    }
                });
    }

    private void saveProfile() {

        fname = mFirstname.getText().toString();
        lname = mLastname.getText().toString();
        phone = mPhone.getText().toString();
        blood = mBlood.getText().toString();

        pfname = mPFirstname.getText().toString();
        plname = mPLastname.getText().toString();
        pphone = mPPhone.getText().toString();
        pblood = mPBlood.getText().toString();

        if (fname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mFirstname.requestFocus();
        } else if (lname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLastname.requestFocus();
        } else if (phone.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPhone.requestFocus();
        } else if (blood.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mBlood.requestFocus();
        } else {
            updateDetails();
        }
    }

    private void updateDetails() {
        String mUrl = getString(R.string.edit_profile_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("firstname", fname);
        requestBody.put("lastname", lname);
        requestBody.put("phone", phone);
        requestBody.put("gender", typeSpinner.getSelectedItem().toString());
        requestBody.put("blood_type", blood);

        requestBody.put("pfirstname", pfname);
        requestBody.put("plastname", plname);
        requestBody.put("pphone", pphone);
        requestBody.put("pgender", typeSpinnerPartner.getSelectedItem().toString());
        requestBody.put("pblood", pblood);

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        String image;
                        try {
                            image = response.getString("image");
                            new UserAccountSetter().updateUserAccount(requestBody, image);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

}
