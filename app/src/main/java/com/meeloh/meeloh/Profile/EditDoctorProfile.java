package com.meeloh.meeloh.Profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.androidnetworking.error.ANError;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.NetworkUtils.VolleyMultipartRequestCallback;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditDoctorProfile extends AppCompatActivity implements View.OnClickListener {

    private TextView mondayS, mondayE, tuesdayS, tuesdayE, wednesdayS, wednesdayE,
            thursdayS, thursdayE, fridayS, fridayE, saturdayS, saturdayE, sundayS,
            sundayE, mondayC, tuesdayC, wednesdayC, thursdayC, fridayC, saturdayC, sundayC;
    private Toolbar mToolbar;
    private User mUser;
    private TextInputEditText mFirstname, mLastname, mPractice, mLicense, mClinicName, mClinicAddress, mPhone;
    private String fname, lname, practice, license, clinicname, clinicaddress, phone;
    private HashMap<String , String > requestBody;
    private ImageButton mUploadImage;    private static int REQUEST_READ_STORAGE = 100;
    private int mIsImagePicked = 0;
    private CircleImageView mImage;
    private String mImagePath;
    private Button saveBtn;
    private int bMonday, bTuesday, bWednesday, bThursday, bFriday, bSaturday, bSunday = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_doctor_profile);

        mUser = AccountsManager.getInstance().getUser();

        mToolbar = findViewById(R.id.edit_profile_toolbar);
        mToolbar.setTitle("Edit profile");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mondayS = findViewById(R.id.mon_start_tv);
        mondayS.setOnClickListener(this);
        mondayE = findViewById(R.id.mon_end_tv);
        mondayE.setOnClickListener(this);
        tuesdayS = findViewById(R.id.tue_start_tv);
        tuesdayS.setOnClickListener(this);
        tuesdayE = findViewById(R.id.tue_end_tv);
        tuesdayE.setOnClickListener(this);
        wednesdayS = findViewById(R.id.wed_start_tv);
        wednesdayS.setOnClickListener(this);
        wednesdayE = findViewById(R.id.wed_end_tv);
        wednesdayE.setOnClickListener(this);
        thursdayS = findViewById(R.id.thu_start_tv);
        thursdayS.setOnClickListener(this);
        thursdayE = findViewById(R.id.thu_end_tv);
        thursdayE.setOnClickListener(this);
        fridayS = findViewById(R.id.fri_start_tv);
        fridayS.setOnClickListener(this);
        fridayE = findViewById(R.id.fri_end_tv);
        fridayE.setOnClickListener(this);
        saturdayS = findViewById(R.id.sat_start_tv);
        saturdayS.setOnClickListener(this);
        saturdayE = findViewById(R.id.sat_end_tv);
        saturdayE.setOnClickListener(this);
        sundayS = findViewById(R.id.sun_start_tv);
        sundayS.setOnClickListener(this);
        sundayE = findViewById(R.id.sun_end_tv);
        sundayE.setOnClickListener(this);
        mondayC = findViewById(R.id.mon_close_tv);
        mondayC.setOnClickListener(this);
        tuesdayC = findViewById(R.id.tue_close_tv);
        tuesdayC.setOnClickListener(this);
        wednesdayC = findViewById(R.id.wed_close_tv);
        wednesdayC.setOnClickListener(this);
        thursdayC = findViewById(R.id.thu_close_tv);
        thursdayC.setOnClickListener(this);
        fridayC = findViewById(R.id.fri_close_tv);
        fridayC.setOnClickListener(this);
        saturdayC = findViewById(R.id.sat_close_tv);
        saturdayC.setOnClickListener(this);
        sundayC = findViewById(R.id.sun_close_tv);
        sundayC.setOnClickListener(this);
        mFirstname = findViewById(R.id.first_name_tiet);
        mLastname = findViewById(R.id.last_name_tiet);
        mPractice = findViewById(R.id.practice_tiet);
        mLicense = findViewById(R.id.license_tiet);
        mClinicName = findViewById(R.id.clinic_name_tiet);
        mClinicAddress = findViewById(R.id.clinic_address_tiet);
        mPhone = findViewById(R.id.phone_tiet);
        mImage = findViewById(R.id.profile_image);
        mUploadImage = findViewById(R.id.edit_profile_btn);
        mUploadImage.setOnClickListener(this);

        saveBtn = findViewById(R.id.save_profile_btn);
        saveBtn.setOnClickListener(this);

        getProfile();

    }

    private void getProfile() {
        String mUrl = getString(R.string.get_user_details_url) + mUser.getId();
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        setprofile(response);
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    @SuppressLint("SetTextI18n")
    private void setprofile(JSONObject response) {
        try {
            mFirstname.setText(response.getString("firstname"));
            mLastname.setText(response.getString("lastname"));
            JSONObject doctor = response.optJSONObject("doctor");

            mPractice.setText(doctor.getString("practice"));
            mLicense.setText(doctor.getString("license"));
            mClinicName.setText(doctor.getString("clinic_name"));
            mClinicAddress.setText(doctor.getString("clinic_address"));
            mPhone.setText(doctor.getString("phone"));

            JSONObject schedule = response.optJSONObject("schedule");

            mondayS.setText(schedule.getString("monday_start"));
            mondayE.setText(schedule.getString("monday_end"));
            setClose(mondayC, schedule.getInt("monday_close"), bMonday);

            tuesdayS.setText(schedule.getString("tuesday_start"));
            tuesdayE.setText(schedule.getString("tuesday_end"));
            setClose(tuesdayC, schedule.getInt("tuesday_close"), bTuesday);

            wednesdayS.setText(schedule.getString("wednesday_start"));
            wednesdayE.setText(schedule.getString("wednesday_end"));
            setClose(wednesdayC, schedule.getInt("wednesday_close"), bWednesday);

            thursdayS.setText(schedule.getString("thursday_start"));
            thursdayE.setText(schedule.getString("thursday_end"));
            setClose(thursdayC, schedule.getInt("thursday_close"), bThursday);

            fridayS.setText(schedule.getString("friday_start"));
            fridayE.setText(schedule.getString("friday_end"));
            setClose(fridayC, schedule.getInt("friday_close"), bFriday);

            saturdayS.setText(schedule.getString("saturday_start"));
            saturdayE.setText(schedule.getString("saturday_end"));
            setClose(saturdayC, schedule.getInt("saturday_close"), bSaturday);

            sundayS.setText(schedule.getString("sunday_start"));
            sundayE.setText(schedule.getString("sunday_end"));
            setClose(sundayC, schedule.getInt("sunday_close"), bSunday);

            Picasso.get().load(doctor.getString("image")).placeholder(R.drawable.user_default).into(mImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setClose(TextView tv, int val, int b) {

        if (tv == mondayC) {
            bMonday = val;
        } else if (tv == tuesdayC) {
            bTuesday = val;
        } else if (tv == wednesdayC) {
            bWednesday = val;
        } else if (tv == thursdayC) {
            bThursday = val;
        } else if (tv == fridayC) {
            bFriday = val;
        } else if (tv == saturdayC) {
            bSaturday = val;
        } else if (tv == sundayC) {
            bSunday = val;
        }

        if (val == 1) {
            tv.setBackgroundResource(R.color.colorPrimary);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            tv.setBackgroundResource(R.color.closedBG);
            tv.setTextColor(Color.parseColor("#99000000"));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mon_start_tv:
                showTimePickerDialog(v, mondayS);
                break;
            case R.id.mon_end_tv:
                showTimePickerDialog(v, mondayE);
                break;
            case R.id.tue_start_tv:
                showTimePickerDialog(v, tuesdayS);
                break;
            case R.id.tue_end_tv:
                showTimePickerDialog(v, tuesdayE);
                break;
            case R.id.wed_start_tv:
                showTimePickerDialog(v, wednesdayS);
                break;
            case R.id.wed_end_tv:
                showTimePickerDialog(v, wednesdayE);
                break;
            case R.id.thu_start_tv:
                showTimePickerDialog(v, thursdayS);
                break;
            case R.id.thu_end_tv:
                showTimePickerDialog(v, thursdayE);
                break;
            case R.id.fri_start_tv:
                showTimePickerDialog(v, fridayS);
                break;
            case R.id.fri_end_tv:
                showTimePickerDialog(v, fridayE);
                break;
            case R.id.sat_start_tv:
                showTimePickerDialog(v, saturdayS);
                break;
            case R.id.sat_end_tv:
                showTimePickerDialog(v, saturdayE);
                break;
            case R.id.sun_start_tv:
                showTimePickerDialog(v, sundayS);
                break;
            case R.id.sun_end_tv:
                showTimePickerDialog(v, sundayE);
                break;
            case R.id.mon_close_tv:
                openDialog(mondayC);
                break;
            case R.id.tue_close_tv:
                openDialog(tuesdayC);
                break;
            case R.id.wed_close_tv:
                openDialog(wednesdayC);
                break;
            case R.id.thu_close_tv:
                openDialog(thursdayC);
                break;
            case R.id.fri_close_tv:
                openDialog(fridayC);
                break;
            case R.id.sat_close_tv:
                openDialog(saturdayC);
                break;
            case R.id.sun_close_tv:
                openDialog(sundayC);
                break;
            case R.id.save_profile_btn:
                saveProfile();
                break;
            case R.id.edit_profile_btn:
                mIsImagePicked = 1;
                openGallery();
                break;
        }
    }

    private void openGallery() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_READ_STORAGE);
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_READ_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_READ_STORAGE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_READ_STORAGE) {
            if (resultCode == RESULT_OK && data.getData() != null) {
                String imagePath;

                Uri selectedImage = data.getData();
                String[] filePathToColumn = new String[]{MediaStore.Images.Media.DATA};

                Cursor cursor = this.getContentResolver().query(selectedImage, filePathToColumn,
                        null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathToColumn[0]);
                imagePath = cursor.getString(columnIndex);
                cursor.close();

                if (mIsImagePicked == 1) {
                    mImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                    mImagePath = imagePath;
                    mIsImagePicked = 0;
                    openUploadImage();
                }
            }
        }
    }

    private void openUploadImage() {

        new SendRequest(this).MultipartRequest(getString(R.string.upload_image_url),
                "image",
                new File(mImagePath),
                new VolleyMultipartRequestCallback() {
                    @Override
                    public void onNetworSuccessResponse(JSONObject response) {

                        mIsImagePicked = 2;
                    }

                    @Override
                    public void onNetworErrorResponse(ANError error) {

                    }
                });
    }

    private void saveProfile() {
        fname = mFirstname.getText().toString();
        lname = mLastname.getText().toString();
        practice = mPractice.getText().toString();
        license = mLicense.getText().toString();
        clinicname = mClinicName.getText().toString();
        clinicaddress = mClinicAddress.getText().toString();
        phone = mPhone.getText().toString();

        if (fname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mFirstname.requestFocus();
        } else if (lname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLastname.requestFocus();
        } else if (practice.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPractice.requestFocus();
        } else if (phone.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPhone.requestFocus();
        } else {
            updateDetails();
        }
    }

    private void updateDetails() {
        String mUrl = getString(R.string.edit_profile_url);
        requestBody = new HashMap<String, String>();
        requestBody.put("firstname", fname);
        requestBody.put("lastname", lname);
        requestBody.put("practice", practice);
        requestBody.put("license", license);
        requestBody.put("clinic_name", clinicname);
        requestBody.put("clinic_address", clinicaddress);
        requestBody.put("phone", phone);

        requestBody.put("monday_start", mondayS.getText().toString());
        requestBody.put("monday_end", mondayE.getText().toString());
        requestBody.put("monday_close", String.valueOf(bMonday));

        requestBody.put("tuesday_start", tuesdayS.getText().toString());
        requestBody.put("tuesday_end", tuesdayE.getText().toString());
        requestBody.put("tuesday_close", String.valueOf(bTuesday));

        requestBody.put("wednesday_start", wednesdayS.getText().toString());
        requestBody.put("wednesday_end", wednesdayE.getText().toString());
        requestBody.put("wednesday_close", String.valueOf(bWednesday));

        requestBody.put("thursday_start", thursdayS.getText().toString());
        requestBody.put("thursday_end", thursdayE.getText().toString());
        requestBody.put("thursday_close", String.valueOf(bThursday));

        requestBody.put("friday_start", fridayS.getText().toString());
        requestBody.put("friday_end", fridayE.getText().toString());
        requestBody.put("friday_close", String.valueOf(bFriday));

        requestBody.put("saturday_start", saturdayS.getText().toString());
        requestBody.put("saturday_end", saturdayE.getText().toString());
        requestBody.put("saturday_close", String.valueOf(bSaturday));

        requestBody.put("sunday_start", sundayS.getText().toString());
        requestBody.put("sunday_end", sundayE.getText().toString());
        requestBody.put("sunday_close", String.valueOf(bSunday));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        String image;
                        try {
                            JSONObject doctor = response.optJSONObject("doctor");
                            image = doctor.getString("image");
                            new UserAccountSetter().updateUserAccount(requestBody, image);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        finish();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    public void showTimePickerDialog(View v, final TextView tv) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(EditDoctorProfile.this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm = "";
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);
                boolean isPM = (selectedHour >= 12);
                tv.setText( String.format("%01d:%02d %s", (selectedHour == 12 || selectedHour == 0) ? 12 : selectedHour % 12, selectedMinute, isPM ? "PM" : "AM"));

                if (tv == mondayS || tv == mondayE) {
                    setClose(mondayC, 0, 0);
                } else if (tv == tuesdayS || tv == tuesdayE) {
                    setClose(tuesdayC, 0, 0);
                } else if (tv == wednesdayS || tv == wednesdayE) {
                    setClose(wednesdayC, 0, 0);
                } else if (tv == thursdayS || tv == thursdayE) {
                    setClose(thursdayC, 0, 0);
                } else if (tv == fridayS || tv == fridayE) {
                    setClose(fridayC, 0, 0);
                } else if (tv == saturdayS || tv == saturdayE) {
                    setClose(saturdayC, 0, 0);
                } else if (tv == sundayS || tv == sundayE) {
                    setClose(sundayC, 0, 0);
                }


            }
        }, hour, minute, false); //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void openDialog(final TextView tv) {

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Close clinic");
        mBuilder.setMessage("Set clinic close");
        mBuilder.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tv.setBackgroundResource(R.color.colorPrimary);
                tv.setTextColor(Color.parseColor("#FFFFFF"));
                if (tv == mondayC) {
                    bMonday = 1;
                } else if (tv == tuesdayC) {
                    bTuesday = 1;
                } else if (tv == wednesdayC) {
                    bWednesday = 1;
                } else if (tv == thursdayC) {
                    bThursday = 1;
                } else if (tv == fridayC) {
                    bFriday = 1;
                } else if (tv == saturdayC) {
                    bSaturday = 1;
                } else if (tv == sundayC) {
                    bSunday = 1;
                }
            }
        });
        mBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = mBuilder.create();
        dialog.show();
    }

}
