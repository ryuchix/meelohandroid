package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.meeloh.meeloh.R;

public class CreateAccountNameActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Button mNext;
    private TextInputEditText mFirstname, mLastname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_name);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mFirstname = findViewById(R.id.first_name_tiet);
        mLastname = findViewById(R.id.last_name_tiet);

        mNext = findViewById(R.id.create_account_name_btn);
        mNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_name_btn:
                next();
                break;
        }
    }

    public void next() {

        String email = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("password");
        Integer usertype = getIntent().getIntExtra("usertype", 0);

        String fname = mFirstname.getText().toString();
        String lname = mLastname.getText().toString();

        if (fname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mFirstname.requestFocus();
        } else if (lname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLastname.requestFocus();
        } else {
            Intent intent = new Intent(this, CreateAccountMedicalPracticeActivity.class);
            intent.putExtra("usertype", usertype);
            intent.putExtra("password", password);
            intent.putExtra("email", email);
            intent.putExtra("firstname", fname);
            intent.putExtra("lastname", lname);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

}
