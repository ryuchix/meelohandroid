package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.AuthLandingActivity;
import com.meeloh.meeloh.Login.LoginActivity;
import com.meeloh.meeloh.MainActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateAccountHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String PASSPORT_TOKEN_KEY = "API_PASSPORT_TOKEN";
    private static final String USER_ID_KEY = "USER_ID";
    private Toolbar mToolbar;
    private Button mNext;
    private String mAboriginal, mTorresStratIslander;
    private RadioGroup gAborinal, gTorresStraitIslander;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_history);

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mNext = findViewById(R.id.create_account_history_btn);
        mNext.setOnClickListener(this);

        gAborinal = findViewById(R.id.aboriginal_rg);
        gTorresStraitIslander = findViewById(R.id.torres_strait_rg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_history_btn:
                next();
                break;
        }
    }

    public void next() {
        String email = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("password");
        Integer usertype = getIntent().getIntExtra("usertype", 0);

        String fname = getIntent().getStringExtra("firstname");
        String lname = getIntent().getStringExtra("lastname");
        String phone = getIntent().getStringExtra("phone");
        String sex = getIntent().getStringExtra("sex");
        String blood = getIntent().getStringExtra("blood");

        String pfname = getIntent().getStringExtra("pfirstname");
        String plname = getIntent().getStringExtra("plastname");
        String pphone = getIntent().getStringExtra("pphone");
        String psex = getIntent().getStringExtra("psex");
        String pblood = getIntent().getStringExtra("pblood");

        if (gAborinal.getCheckedRadioButtonId() == -1 || gTorresStraitIslander.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, "Your answer is required.", Toast.LENGTH_SHORT).show();
        } else {
            int id = gAborinal.getCheckedRadioButtonId();
            View radioButton = gAborinal.findViewById(id);
            int radioId = gAborinal.indexOfChild(radioButton);
            RadioButton btn = (RadioButton) gAborinal.getChildAt(radioId);
            mAboriginal = (String) btn.getText();

            int id2 = gTorresStraitIslander.getCheckedRadioButtonId();
            View radioButton2 = gTorresStraitIslander.findViewById(id2);
            int radioId2 = gTorresStraitIslander.indexOfChild(radioButton2);
            RadioButton btn2 = (RadioButton) gTorresStraitIslander.getChildAt(radioId2);
            mTorresStratIslander = (String) btn2.getText();

            Map<String, String> requestBody = new HashMap<String, String>();
            requestBody.put("email", email);
            requestBody.put("password", password);
            requestBody.put("usertype", String.valueOf(usertype));

            requestBody.put("firstname", fname);
            requestBody.put("lastname", lname);
            requestBody.put("phone", phone);
            requestBody.put("gender", sex);
            requestBody.put("blood_type", blood);
            requestBody.put("aboriginal", mAboriginal.equals("Yes") ? "1" : "0");
            requestBody.put("torres_strait_islander", mTorresStratIslander.equals("Yes") ? "1" : "0");

            requestBody.put("pfirstname", pfname);
            requestBody.put("plastname", plname);
            requestBody.put("pphone", pphone);
            requestBody.put("psex", psex);
            requestBody.put("pblood", pblood);

            new SendRequest(this).JsonPostRequestNoAuth(getString(R.string.create_account_url),
                    new JSONObject(requestBody),
                    new VolleyJsonPostRequestCallback() {
                        @Override
                        public void onJsonPostRequestSuccess(JSONObject response) {
                            try {
                                setup(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onJsonPostRequestError(VolleyError error) {
                            //
                        }
                    });

        }
    }

    private void setup(JSONObject response) throws JSONException {
        User user = new UserAccountSetter(response).setupUserAccount();
        AccountsManager.getInstance().setUser(user);

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.putString(PASSPORT_TOKEN_KEY,
                response.getString("token"));
        mSharedPreferencesEditor.putString(USER_ID_KEY,
                response.getString("id"));
        mSharedPreferencesEditor.commit();

        Intent intent = new Intent(getApplicationContext(), AuthLandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();
    }
}

