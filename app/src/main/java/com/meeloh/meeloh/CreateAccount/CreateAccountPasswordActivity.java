package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.meeloh.meeloh.R;

public class CreateAccountPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Button mNext;
    private TextInputEditText mPassword, mCPassword;
    private String password, cPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_password);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mNext = findViewById(R.id.create_account_password_btn);
        mNext.setOnClickListener(this);
        mPassword = findViewById(R.id.password_tiet);
        mCPassword = findViewById(R.id.password_c_tiet);

        viewPassword();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_password_btn:
                next();
                break;
        }
    }

    public void next() {
        password = mPassword.getText().toString();
        cPassword = mCPassword.getText().toString();
        if(password.trim().length() == 0 || cPassword.trim().length() == 0) {
            Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
            mPassword.requestFocus();
            mCPassword.requestFocus();
        } else if (!cPassword.equals(password)) {
            Toast.makeText(this, "Confirm password did not match.", Toast.LENGTH_SHORT).show();
            mCPassword.requestFocus();
        } else {
            String email = getIntent().getStringExtra("email");
            Intent intent = new Intent(this, CreateAccountSelectionActivity.class);
            intent.putExtra("password", password);
            intent.putExtra("email", email);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    public void viewPassword() {
        mPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getX() >= (mPassword.getRight() - mPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mPassword.setTransformationMethod(null);
                        return true;
                    }
                } else {
                    mPassword.setTransformationMethod(new PasswordTransformationMethod());

                }
                return false;
            }
        });

        mCPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getX() >= (mCPassword.getRight() - mCPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mCPassword.setTransformationMethod(null);
                        return true;
                    }
                } else {
                    mCPassword.setTransformationMethod(new PasswordTransformationMethod());

                }
                return false;
            }
        });
    }
}
