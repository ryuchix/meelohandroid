package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.meeloh.meeloh.R;

public class CreateAccountSelectionActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Button mNext;
    private TextView mDoctor, mParent;
    private Integer select = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_selection);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mNext = findViewById(R.id.create_account_selection_btn);
        mNext.setClickable(false);
        mNext.setOnClickListener(this);
        mDoctor = findViewById(R.id.select_doctor_btn);
        mDoctor.setOnClickListener(this);
        mParent = findViewById(R.id.select_parent_btn);
        mParent.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_selection_btn:
                if (mNext.isClickable()) {
                    next();
                } else {
                    Toast.makeText(this, "Please select a status.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.select_doctor_btn:
                doctor();
                break;
            case R.id.select_parent_btn:
                parent();
                break;
        }
    }

    private void parent() {
        select = 1;
        mParent.setBackgroundResource(R.drawable.textview_border_right);
        mParent.setTextColor(Color.parseColor("#fafafa"));
        mDoctor.setBackgroundResource(R.drawable.textview_border2);
        mDoctor.setTextColor(Color.parseColor("#000000"));
        mNext.setClickable(true);
    }

    private void doctor() {
        select = 2;
        mDoctor.setBackgroundResource(R.drawable.textview_border);
        mDoctor.setTextColor(Color.parseColor("#fafafa"));
        mParent.setBackgroundResource(R.drawable.textview_border_left);
        mParent.setTextColor(Color.parseColor("#000000"));
        mNext.setClickable(true);
    }

    public void next() {
        String email = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("password");
        if (select.equals(1)) {
            Intent intent = new Intent(this, CreateAccountDetailsActivity.class);
            intent.putExtra("usertype", 1);
            intent.putExtra("password", password);
            intent.putExtra("email", email);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else if (select.equals(2)) {
            Intent intent = new Intent(this, CreateAccountNameActivity.class);
            intent.putExtra("usertype", 2);
            intent.putExtra("password", password);
            intent.putExtra("email", email);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please select a status.", Toast.LENGTH_SHORT).show();
        }
    }

}
