package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.AccountsManager;
import com.meeloh.meeloh.AuthLandingActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateAccountMedicalPracticeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String PASSPORT_TOKEN_KEY = "API_PASSPORT_TOKEN";
    private static final String USER_ID_KEY = "USER_ID";

    private Toolbar mToolbar;
    private Button mNext;
    private TextInputEditText mPractice, mLicense;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_medical_practice);

        mSharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mPractice = findViewById(R.id.practice_tiet);
        mLicense = findViewById(R.id.license_tiet);

        mNext = findViewById(R.id.create_account_medical_practice_btn);
        mNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_medical_practice_btn:
                next();
                break;
        }
    }

    public void next() {

        String practice = mPractice.getText().toString();
        String license = mLicense.getText().toString();

        String email = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("password");
        Integer usertype = getIntent().getIntExtra("usertype", 0);
        String fname = getIntent().getStringExtra("firstname");
        String lname = getIntent().getStringExtra("lastname");

        if (practice.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPractice.requestFocus();
        } else if (license.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLicense.requestFocus();
        } else {

            Map<String, String> requestBody = new HashMap<String, String>();
            requestBody.put("email", email);
            requestBody.put("password", password);
            requestBody.put("usertype", String.valueOf(usertype));

            requestBody.put("firstname", fname);
            requestBody.put("lastname", lname);
            requestBody.put("practice", practice);
            requestBody.put("license", license);

            new SendRequest(this).JsonPostRequestNoAuth(getString(R.string.create_account_url),
                    new JSONObject(requestBody),
                    new VolleyJsonPostRequestCallback() {
                        @Override
                        public void onJsonPostRequestSuccess(JSONObject response) {
                            try {
                                setup(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onJsonPostRequestError(VolleyError error) {
                            //
                        }
                    });
        }
    }

    private void setup(JSONObject response) throws JSONException {
        User user = new UserAccountSetter(response).setupUserAccount();
        AccountsManager.getInstance().setUser(user);

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.putString(PASSPORT_TOKEN_KEY,
                response.getString("token"));
        mSharedPreferencesEditor.putString(USER_ID_KEY,
                response.getString("id"));
        mSharedPreferencesEditor.commit();

        Intent intent = new Intent(getApplicationContext(), AuthLandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();
    }
}
