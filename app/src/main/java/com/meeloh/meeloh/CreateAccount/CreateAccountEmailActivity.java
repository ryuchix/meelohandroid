package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Login.LoginActivity;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CreateAccountEmailActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Button mNext;
    private TextInputEditText mEmail;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_email);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mNext = findViewById(R.id.create_account_email_btn);
        mNext.setOnClickListener(this);

        mEmail = findViewById(R.id.email_address_tiet);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_email_btn:
                next();
                break;
        }
    }

    public void next() {
        email = mEmail.getText().toString();

        if(email.trim().length() == 0) {
            Toast.makeText(this, "Enter email address", Toast.LENGTH_SHORT).show();
            mEmail.requestFocus();
        } else {
            Map<String, String> requestBody = new HashMap<String, String>();
            requestBody.put("email", email);

            new SendRequest(this).JsonPostRequestNoAuth(getString(R.string.validate_email_url),
                    new JSONObject(requestBody),
                    new VolleyJsonPostRequestCallback() {
                        @Override
                        public void onJsonPostRequestSuccess(JSONObject response) {
                            try {
                                if (response.getInt("message") == 1) {
                                    Toast.makeText(CreateAccountEmailActivity.this, "Email address already exists", Toast.LENGTH_LONG).show();
                                } else {
                                    Intent intent = new Intent(CreateAccountEmailActivity.this, CreateAccountPasswordActivity.class);
                                    intent.putExtra("email", email);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onJsonPostRequestError(VolleyError error) {

                        }
                    });

        }

    }

}
