package com.meeloh.meeloh.CreateAccount;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.meeloh.meeloh.R;

public class CreateAccountDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Button mNext;

    private String TAG = "CreateAccountDetailsActivity";
    private LayoutInflater mInflator;
    private Spinner typeSpinner;
    private Boolean selected;
    private TextInputEditText mFirstname, mLastname, mPhone, mBlood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_details);

        mToolbar = findViewById(R.id.create_account_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mNext = findViewById(R.id.create_account_details_btn);
        mNext.setOnClickListener(this);

        selected = false;
        typeSpinner = (Spinner) findViewById(R.id.spinner);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(typeSelectedListener);
        typeSpinner.setOnTouchListener(typeSpinnerTouchListener);
        mInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        mFirstname = findViewById(R.id.first_name_tiet);
        mLastname = findViewById(R.id.last_name_tiet);
        mPhone = findViewById(R.id.phone_tiet);
        mBlood = findViewById(R.id.blood_type_tiet);

    }

    private SpinnerAdapter typeSpinnerAdapter = new BaseAdapter() {

        private TextView text;
        private String[] data = { "Male", "Female" };
        private int count = 2;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);
            if (!selected) {
                text.setText("Sex");

            } else {
                text.setText(data[position]);
            }
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return count;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            Log.d(TAG, "user selected : "
                    + typeSpinner.getSelectedItem().toString());

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            return false;
        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_account_details_btn:
                next();
                break;
        }
    }

    public void next() {

        String fname = mFirstname.getText().toString();
        String lname = mLastname.getText().toString();
        String phone = mPhone.getText().toString();
        String sex = typeSpinner.getSelectedItem().toString();
        String blood = mBlood.getText().toString();

        String email = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("password");
        Integer usertype = getIntent().getIntExtra("usertype", 0);

        if (fname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mFirstname.requestFocus();
        } else if (lname.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLastname.requestFocus();
        } else if (phone.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPhone.requestFocus();
        } else if (blood.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mBlood.requestFocus();
        } else {
            Intent intent = new Intent(this, CreateAccountPartnerActivity.class);
            intent.putExtra("usertype", usertype);
            intent.putExtra("password", password);
            intent.putExtra("email", email);
            intent.putExtra("firstname", fname);
            intent.putExtra("lastname", lname);
            intent.putExtra("phone", phone);
            intent.putExtra("sex", sex);
            intent.putExtra("blood", blood);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }
}
