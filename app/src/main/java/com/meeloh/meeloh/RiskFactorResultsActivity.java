package com.meeloh.meeloh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.meeloh.meeloh.Models.User;

public class RiskFactorResultsActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mEditBtn, mProfileBtn;
    private Toolbar mToolbar;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_risk_factor_results);

        mUser = AccountsManager.getInstance().getUser();

        mToolbar = findViewById(R.id.risk_factor_results_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mEditBtn = findViewById(R.id.edit_answers_btn);
        mEditBtn.setOnClickListener(this);
        mProfileBtn = findViewById(R.id.go_to_profile_btn);
        mProfileBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_answers_btn:
                editAnswer();
                break;
            case R.id.go_to_profile_btn:
                gotoProfile();
                break;
        }
    }

    private void gotoProfile() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();
    }

    private void editAnswer() {
        Intent intent = new Intent(getApplicationContext(), EditRiskFactorQuestionsActivity.class);
        intent.putExtra("userid", mUser.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
