package com.meeloh.meeloh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.meeloh.meeloh.CreateAccount.CreateAccountEmailActivity;

public class AuthLandingActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mContinueBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_landing);

        mContinueBtn = findViewById(R.id.continue_btn);
        mContinueBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.continue_btn:
                next();
        }
    }

    private void next() {
        Intent intent = new Intent(this, RiskFactorQuestionsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
