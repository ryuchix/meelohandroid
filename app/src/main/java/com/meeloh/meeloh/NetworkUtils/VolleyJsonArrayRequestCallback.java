package com.meeloh.meeloh.NetworkUtils;

import com.android.volley.VolleyError;

import org.json.JSONArray;

public interface VolleyJsonArrayRequestCallback {
    void onRequestSuccess(JSONArray response);
    void onRequestFailed(VolleyError error);
}
