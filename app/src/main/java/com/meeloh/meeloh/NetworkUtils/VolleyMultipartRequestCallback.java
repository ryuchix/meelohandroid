package com.meeloh.meeloh.NetworkUtils;

import com.androidnetworking.error.ANError;

import org.json.JSONObject;

public interface VolleyMultipartRequestCallback {
    void onNetworSuccessResponse(JSONObject response);
    void onNetworErrorResponse(ANError error);
}
