package com.meeloh.meeloh.NetworkUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.meeloh.meeloh.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class SendRequest {
    private static final String API_TOKEN = "API_PASSPORT_TOKEN";
    private static final String USER_ID_KEY = "USER_ID";

    private static RequestQueue queue;
    private static SharedPreferences preferences;
    private static String mApiToken;
    private static int mUserId;
    private static Context mContext;

    public SendRequest(Context context) {
        mContext = context;
        queue = VolleySingleton.getInstance(context).getQueue();
        preferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        mApiToken = preferences.getString(API_TOKEN, "");
        mUserId = preferences.getInt("USER_ID_KEY", 0);
    }

    public static void JsonArrayGetRequest(String url,
                                           final VolleyJsonArrayRequestCallback callback) {

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        callback.onRequestSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onRequestFailed(error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + mApiToken);
                return headers;
            }
        };

        getRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(getRequest);
    }

    public static void JsonPostRequestWithAuth(String mUrl, JSONObject mRequestBody,
                                               final VolleyJsonPostRequestCallback callback) {

        JsonObjectRequest postRequest =
                new JsonObjectRequest(Request.Method.POST, mUrl, mRequestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                callback.onJsonPostRequestSuccess(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                callback.onJsonPostRequestError(error);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Authorization", "Bearer " + mApiToken);
                        return headers;
                    }
                };

        queue.add(postRequest);
    }

    public static void JsonDeleteRequestWithAuth(String mUrl, JSONObject mRequestBody,
                                                 final VolleyJsonPostRequestCallback callback) {

        JsonObjectRequest postRequest =
                new JsonObjectRequest(Request.Method.DELETE, mUrl, mRequestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                callback.onJsonPostRequestSuccess(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                callback.onJsonPostRequestError(error);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Authorization", "Bearer " + mApiToken);
                        return headers;
                    }
                };

        queue.add(postRequest);
    }

    public static void JsonPostRequestNoAuth(String mUrl, JSONObject mRequestBody,
                                             final VolleyJsonPostRequestCallback callback) {

        JsonObjectRequest postRequest =
                new JsonObjectRequest(Request.Method.POST, mUrl, mRequestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                callback.onJsonPostRequestSuccess(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                callback.onJsonPostRequestError(error);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json; charset=utf-8");
                        return headers;
                    }
                };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    public static void JsonPutRequest(String mUrl, JSONObject mRequestBody,
                                      final VolleyJsonPostRequestCallback callback) {

        JsonObjectRequest postRequest =
                new JsonObjectRequest(Request.Method.PUT, mUrl, mRequestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                callback.onJsonPostRequestSuccess(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                callback.onJsonPostRequestError(error);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Authorization", "Bearer " + mApiToken);
                        return headers;
                    }
                };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    public static void JsonGetRequest(String mUrl, final VolleyJsonPostRequestCallback callback) {

        JsonObjectRequest getRequest = new JsonObjectRequest(
                Request.Method.GET,
                mUrl,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onJsonPostRequestSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onJsonPostRequestError(error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer " + mApiToken);
                return headers;
            }
        };

        getRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(getRequest);
    }

    public static void MultipartRequest(String mUrl,
                                        final File image,
                                        final ProgressBar progressBar,
                                        final VolleyMultipartRequestCallback callback) {

        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.initialize(mContext.getApplicationContext());

        AndroidNetworking.upload(mUrl)
                .addHeaders("Authorization", "Bearer " + mApiToken)
                .addMultipartFile("profile_image", image)
                .setPriority(Priority.HIGH)
                .setTag("UPLOAD IMAGE")
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long l, long l1) {
                        int percentage = (int) ((l1 / l) * 100);
                        progressBar.setProgress(percentage);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        callback.onNetworSuccessResponse(jsonObject);
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        callback.onNetworErrorResponse(anError);
                    }
                });
    }

    public static void MultipartRequest(String mUrl,
                                        final String key,
                                        final File image,
                                        final VolleyMultipartRequestCallback callback) {

        AndroidNetworking.initialize(mContext.getApplicationContext());

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        AndroidNetworking
                .upload(mUrl)
                .addHeaders("Authorization", "Bearer " + mApiToken)
                .addMultipartFile(key, image)
                .setPriority(Priority.HIGH)
                .setTag("UPLOAD IMAGE")
                .setOkHttpClient(okHttpClient)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        callback.onNetworSuccessResponse(jsonObject);
                    }

                    @Override
                    public void onError(ANError anError) {
                        callback.onNetworErrorResponse(anError);
                    }
                });
    }


}
