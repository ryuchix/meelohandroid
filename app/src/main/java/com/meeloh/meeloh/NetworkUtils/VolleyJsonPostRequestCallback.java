package com.meeloh.meeloh.NetworkUtils;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyJsonPostRequestCallback {
    void onJsonPostRequestSuccess(JSONObject response);
    void onJsonPostRequestError(VolleyError error);
}
