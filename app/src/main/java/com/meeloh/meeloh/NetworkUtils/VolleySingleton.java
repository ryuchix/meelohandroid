package com.meeloh.meeloh.NetworkUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
    private static VolleySingleton instance;
    private RequestQueue queue;
    private ImageLoader imageLoader;
    private static Context context;

    private VolleySingleton(Context context) {
        this.context = context;

        queue = getQueue();

        imageLoader = new ImageLoader(queue, new ImageLoader.ImageCache() {
            final int cachesize = (int) ((Runtime.getRuntime().maxMemory()/1024)/8);
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(cachesize);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }

    public static synchronized VolleySingleton getInstance(Context context) {
        if (instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }

    public RequestQueue getQueue() {
        if(queue==null) {
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return queue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}