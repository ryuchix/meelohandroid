package com.meeloh.meeloh.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class InformationAlertDialog extends DialogFragment {

    public static final InformationAlertDialog newInstance(String message) {
        InformationAlertDialog informationAlertDialog = new InformationAlertDialog();
        Bundle bundle = new Bundle(1);
        bundle.putString("message", message);
        informationAlertDialog.setArguments(bundle);
        return informationAlertDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Oops!")
                .setMessage(getArguments().getString("message"))
                .setPositiveButton( "OKAY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Dismiss dialog
                    }
                });

        return builder.create();
    }
}