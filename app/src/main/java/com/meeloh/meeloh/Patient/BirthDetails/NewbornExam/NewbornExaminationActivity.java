package com.meeloh.meeloh.Patient.BirthDetails.NewbornExam;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.Patient.BirthDetails.Maternal.MaternalInformationActivity;
import com.meeloh.meeloh.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class NewbornExaminationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Integer childId;
    private Button mSave;
    private HashMap<String , String > requestBody;
    private RadioGroup mRadioGroup1, mRadioGroup2, mRadioGroup3, mRadioGroup4, mRadioGroup5, mRadioGroup6, mRadioGroup7, mRadioGroup8;
    private RadioButton mRadioButton1a, mRadioButton1b, mRadioButton2a, mRadioButton2b, mRadioButton3a, mRadioButton3b, mRadioButton4a, mRadioButton4b, mRadioButton5a, mRadioButton5b, mRadioButton6a, mRadioButton6b, mRadioButton7a, mRadioButton7b, mRadioButton8a, mRadioButton8b;
    private RadioButton mRadioButton1, mRadioButton2, mRadioButton3, mRadioButton4, mRadioButton5, mRadioButton6, mRadioButton7, mRadioButton8;
    private TextView mRg1, mRg2, mRg3, mRg4, mRg5, mRg6, mRg7, mRg8;
    private TextInputEditText mField1, mField2, mField3, mField4;
    private DatePickerDialog picker;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String TAG = "EditProfileActivity";
    private LayoutInflater mInflator;
    private Spinner typeSpinner;
    private Boolean selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newborn_examination2);

        requestBody = new HashMap<>();
        childId = getIntent().getIntExtra("children_id", 0);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Newborn Examination");
        mToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        selected = false;

        typeSpinner = (Spinner) findViewById(R.id.spinner);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(typeSelectedListener);
        typeSpinner.setOnTouchListener(typeSpinnerTouchListener);
        mInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        mSave = findViewById(R.id.save_btn);
        mSave.setOnClickListener(this);

        mRg1 = findViewById(R.id.rg_1_tv);
        mRg1.setOnClickListener(this);
        mRg2 = findViewById(R.id.rg_2_tv);
        mRg2.setOnClickListener(this);
        mRg3 = findViewById(R.id.rg_3_tv);
        mRg3.setOnClickListener(this);
        mRg4 = findViewById(R.id.rg_4_tv);
        mRg4.setOnClickListener(this);
        mRg5 = findViewById(R.id.rg_5_tv);
        mRg5.setOnClickListener(this);
        mRg6 = findViewById(R.id.rg_6_tv);
        mRg6.setOnClickListener(this);
        mRg7 = findViewById(R.id.rg_7_tv);
        mRg7.setOnClickListener(this);
        mRg8 = findViewById(R.id.rg_8_tv);
        mRg8.setOnClickListener(this);

        mRadioGroup1 = findViewById(R.id.rg_1);
        mRadioGroup2 = findViewById(R.id.rg_2);
        mRadioGroup3 = findViewById(R.id.rg_3);
        mRadioGroup4 = findViewById(R.id.rg_4);
        mRadioGroup5 = findViewById(R.id.rg_5);
        mRadioGroup6 = findViewById(R.id.rg_6);
        mRadioGroup7 = findViewById(R.id.rg_7);
        mRadioGroup8 = findViewById(R.id.rg_8);

        mRadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton1 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton2 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton3 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton4 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton5 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton6 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton7 = (RadioButton) findViewById(checkedId);
            }
        });

        mRadioGroup8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mRadioButton8 = (RadioButton) findViewById(checkedId);
            }
        });

        mField1 = findViewById(R.id.babys_name_tiet);
        mField2 = findViewById(R.id.date_of_birth_tiet);
        mField2.setOnClickListener(this);
        mField3 = findViewById(R.id.babys_age_tiet);

    }

    private SpinnerAdapter typeSpinnerAdapter = new BaseAdapter() {
        private TextView text;
        private String[] data = { "Male", "Female" };
        private int count = 2;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);
            if (!selected) {
                text.setText("Sex");
                text.setTextColor(Color.GRAY);
            } else {
                text.setText(data[position]);
                text.setTextColor(Color.BLACK);
            }
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return count;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            return false;
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_btn:
                saveDetails();
                break;
            case R.id.rg_1_tv:
                openModal(mRg1);
                break;
            case R.id.rg_2_tv:
                openModal(mRg2);
                break;
            case R.id.rg_3_tv:
                openModal(mRg3);
                break;
            case R.id.rg_4_tv:
                openModal(mRg4);
                break;
            case R.id.rg_5_tv:
                openModal(mRg5);
                break;
            case R.id.rg_6_tv:
                openModal(mRg6);
                break;
            case R.id.rg_7_tv:
                openModal(mRg7);
                break;
            case R.id.rg_8_tv:
                openModal(mRg8);
                break;
            case R.id.date_of_birth_tiet:
                openDatePicker();
                break;
        }
    }

    private void openModal(final TextView tv) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Add comment");
        dialogBuilder.setCancelable(false);
        final View customLayout = getLayoutInflater().inflate(R.layout.content_add_comment, null);
        dialogBuilder.setView(customLayout);

        final EditText editText = (EditText) customLayout.findViewById(R.id.edittext);

        if (tv.getText().toString().equals("add comment") || tv.getText().toString().equals("ADD COMMENT")) {
            editText.setText("");
        } else {
            editText.setText(tv.getText().toString());
        }

        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String text = editText.getText().toString();
                if (text.trim().length() == 0 || text.equals("ADD COMMENT")) {
                    tv.setTextColor(ContextCompat.getColor(NewbornExaminationActivity.this, R.color.colorPrimary));
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_circle_outline_black_24dp, 0, 0, 0);
                    tv.setText("ADD COMMENT");
                } else {
                    tv.setTextColor(ContextCompat.getColor(NewbornExaminationActivity.this, R.color.disableText));
                    tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    tv.setText(text);
                }
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        dialogBuilder.create();
        dialogBuilder.show();
    }

    private void saveDetails() {
        String mUrl = getString(R.string.get_newborn_examination) + childId;
        requestBody = new HashMap<String, String>();
        requestBody.put("children_id", String.valueOf(childId));
        requestBody.put("name", mField1.getText().toString());
        requestBody.put("birth_date", mField2.getText().toString());
        requestBody.put("age", mField3.getText().toString());
        requestBody.put("gender", typeSpinner.getSelectedItem().toString());
        requestBody.put("q1", String.valueOf(mRadioGroup1.getCheckedRadioButtonId() == -1 ? "" : mRadioButton1.getText()));
        requestBody.put("q1_comment", mRg1.getText().toString().equals("add comment") ? null : mRg1.getText().toString());
        requestBody.put("q2", String.valueOf(mRadioGroup2.getCheckedRadioButtonId() == -1 ? "" : mRadioButton2.getText()));
        requestBody.put("q2_comment", mRg2.getText().toString().equals("add comment") ? null : mRg2.getText().toString());
        requestBody.put("q3", String.valueOf(mRadioGroup3.getCheckedRadioButtonId() == -1 ? "" : mRadioButton3.getText()));
        requestBody.put("q3_comment", mRg3.getText().toString().equals("add comment") ? null : mRg3.getText().toString());
        requestBody.put("q4", String.valueOf(mRadioGroup4.getCheckedRadioButtonId() == -1 ? "" : mRadioButton4.getText()));
        requestBody.put("q4_comment", mRg4.getText().toString().equals("add comment") ? null : mRg4.getText().toString());
        requestBody.put("q5", String.valueOf(mRadioGroup5.getCheckedRadioButtonId() == -1 ? "" : mRadioButton5.getText()));
        requestBody.put("q5_comment", mRg5.getText().toString().equals("add comment") ? null : mRg5.getText().toString());
        requestBody.put("q6", String.valueOf(mRadioGroup6.getCheckedRadioButtonId() == -1 ? "" : mRadioButton6.getText()));
        requestBody.put("q6_comment", mRg6.getText().toString().equals("add comment") ? null : mRg6.getText().toString());
        requestBody.put("q7", String.valueOf(mRadioGroup7.getCheckedRadioButtonId() == -1 ? "" : mRadioButton7.getText()));
        requestBody.put("q7_comment", mRg7.getText().toString().equals("add comment") ? null : mRg7.getText().toString());
        requestBody.put("q8", String.valueOf(mRadioGroup8.getCheckedRadioButtonId() == -1 ? "" : mRadioButton8.getText()));
        requestBody.put("q8_comment", mRg8.getText().toString().equals("add comment") ? null : mRg8.getText().toString());
        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(NewbornExaminationActivity.this, "Data saved.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    @Override
    protected void onResume() {
        getNewnbornExamination();
        super.onResume();
    }

    private void getNewnbornExamination() {
        String mUrl = getString(R.string.get_newborn_examination) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        if (response.length() > 0) {
                            setNewnbornExamination(response);
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setNewnbornExamination(JSONObject response) {
        try {
            mField1.setText(response.getString("name").equals("null") ? "" : response.getString("name"));
            mField2.setText(response.getString("birth_date").equals("null") ? "" : response.getString("birth_date"));
            mField3.setText(response.getString("age").equals("null") ? "" : response.getString("age"));
            String gender = response.getString("gender");
            typeSpinner.setAdapter(typeSpinnerAdapter);
            if (gender.equals("Male")) {
                selected = true;
                typeSpinner.setSelection(0);
            } else {
                selected = true;
                typeSpinner.setSelection(1);
            }

            if (response.getString("q1").equals("Normal")) {
                mRadioGroup1.check(R.id.rg_1_normal);
            } else {
                if (response.getString("q1") == null || response.getString("q1").equals("null")) {
                    mRadioGroup1.clearCheck();
                } else {
                    mRadioGroup1.check(R.id.rg_1_notnormal);
                }
            }

            if (response.getString("q2").equals("Normal")) {
                mRadioGroup2.check(R.id.rg_2_normal);
            } else {
                if (response.getString("q2") == null || response.getString("q2").equals("null")) {
                    mRadioGroup2.clearCheck();
                } else {
                    mRadioGroup2.check(R.id.rg_2_notnormal);
                }
            }

            if (response.getString("q3").equals("Normal")) {
                mRadioGroup3.check(R.id.rg_3_normal);
            } else {
                if (response.getString("q3") == null || response.getString("q3").equals("null")) {
                    mRadioGroup3.clearCheck();
                } else {
                    mRadioGroup3.check(R.id.rg_3_notnormal);
                }
            }

            if (response.getString("q4").equals("Normal")) {
                mRadioGroup4.check(R.id.rg_4_normal);
            } else {
                if (response.getString("q4") == null || response.getString("q4").equals("null")) {
                    mRadioGroup4.clearCheck();
                } else {
                    mRadioGroup4.check(R.id.rg_4_notnormal);
                }
            }

            if (response.getString("q5").equals("Normal")) {
                mRadioGroup5.check(R.id.rg_3_normal);
            } else {
                if (response.getString("q5") == null || response.getString("q5").equals("null")) {
                    mRadioGroup5.clearCheck();
                } else {
                    mRadioGroup5.check(R.id.rg_5_notnormal);
                }
            }

            if (response.getString("q6").equals("Normal")) {
                mRadioGroup6.check(R.id.rg_6_normal);
            } else {
                if (response.getString("q6") == null || response.getString("q6").equals("null")) {
                    mRadioGroup6.clearCheck();
                } else {
                    mRadioGroup6.check(R.id.rg_6_notnormal);
                }
            }

            if (response.getString("q7").equals("Normal")) {
                mRadioGroup7.check(R.id.rg_7_normal);
            } else {
                if (response.getString("q7") == null || response.getString("q7").equals("null")) {
                    mRadioGroup7.clearCheck();
                } else {
                    mRadioGroup7.check(R.id.rg_7_notnormal);
                }
            }

            if (response.getString("q8").equals("Normal")) {
                mRadioGroup8.check(R.id.rg_8_normal);
            } else {
                if (response.getString("q8") == null || response.getString("q8").equals("null")) {
                    mRadioGroup8.clearCheck();
                } else {
                    mRadioGroup8.check(R.id.rg_8_notnormal);
                }
            }

            mRg1.setText(response.getString("q1_comment").equals("null") ? "add comment" : response.getString("q1_comment"));
            mRg2.setText(response.getString("q2_comment").equals("null") ? "add comment" : response.getString("q2_comment"));
            mRg3.setText(response.getString("q3_comment").equals("null") ? "add comment" : response.getString("q3_comment"));
            mRg4.setText(response.getString("q4_comment").equals("null") ? "add comment" : response.getString("q4_comment"));
            mRg5.setText(response.getString("q5_comment").equals("null") ? "add comment" : response.getString("q5_comment"));
            mRg6.setText(response.getString("q6_comment").equals("null") ? "add comment" : response.getString("q6_comment"));
            mRg7.setText(response.getString("q7_comment").equals("null") ? "add comment" : response.getString("q7_comment"));
            mRg8.setText(response.getString("q8_comment").equals("null") ? "add comment" : response.getString("q8_comment"));

            if (!response.getString("q1_comment").equals("null")) {
                mRg1.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            }
            if (!response.getString("q2_comment").equals("null")) {
                mRg2.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (!response.getString("q3_comment").equals("null")) {
                mRg3.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (!response.getString("q4_comment").equals("null")) {
                mRg4.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg4.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (!response.getString("q5_comment").equals("null")) {
                mRg5.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg5.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (!response.getString("q6_comment").equals("null")) {
                mRg6.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg6.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (!response.getString("q7_comment").equals("null")) {
                mRg7.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg7.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            if (!response.getString("q8_comment").equals("null")) {
                mRg8.setTextColor(ContextCompat.getColor(this, R.color.disableText));
                mRg8.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        final SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        final String month_name = month_date.format(cldr.getTime());
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        final Calendar today = Calendar.getInstance();
        // date picker dialog
        picker = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mField2.setText(MONTHS[monthOfYear] + " " + dayOfMonth + ", " + year);

                        cldr.set(year, monthOfYear, dayOfMonth);
                        int age = today.get(Calendar.YEAR) - cldr.get(Calendar.YEAR);
                        if (today.get(Calendar.DAY_OF_YEAR) < cldr.get(Calendar.DAY_OF_YEAR)){
                            age--;
                        }
                        mField3.setText(String.valueOf(age));
                    }
                }, year, month, day);
        picker.show();
    }
}
