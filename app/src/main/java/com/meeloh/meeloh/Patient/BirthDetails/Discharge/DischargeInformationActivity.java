package com.meeloh.meeloh.Patient.BirthDetails.Discharge;

import android.annotation.SuppressLint;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.Patient.BirthDetails.Maternal.MaternalInformationActivity;
import com.meeloh.meeloh.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DischargeInformationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Integer childId;
    private int birthDetailsId;
    private TextView goBack;
    private Button mSave;
    private HashMap<String , String > requestBody;
    private TextInputEditText mPostPartum, mFeeding, mDifficulties, mDischargeDate, mDischargeWeight, mDischargeCirc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discharge_information);

        requestBody = new HashMap<>();
        childId = getIntent().getIntExtra("children_id", 0);
        birthDetailsId = getIntent().getIntExtra("birth_details_id", 0);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Birth details");
        mToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        goBack = findViewById(R.id.goback_tv);
        goBack.setOnClickListener(this);
        mPostPartum = findViewById(R.id.postpartum_tiet);
        mFeeding = findViewById(R.id.feeding_discharge_tiet);
        mDifficulties = findViewById(R.id.difficulties_with_feeding_tiet);
        mDischargeDate = findViewById(R.id.discharge_date_tiet);
        mDischargeWeight = findViewById(R.id.discharge_weight_tiet);
        mDischargeCirc = findViewById(R.id.head_circ_tiet);
        mSave = findViewById(R.id.save_btn);
        mSave.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        getDischarge();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void getDischarge() {
        String mUrl = getString(R.string.get_discharge_info) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        if (response.length() > 0) {
                            setDischarge(response);
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setDischarge(JSONObject response) {
        try {
            mPostPartum.setText(response.getString("post_partum_complications").equals("null") ? "" : response.getString("post_partum_complications"));
            mFeeding.setText(response.getString("feeding_at_discharge").equals("null") ? "" : response.getString("feeding_at_discharge"));
            mDifficulties.setText(response.getString("difficulties_with_feeding").equals("null") ? "" : response.getString("difficulties_with_feeding"));
            mDischargeDate.setText(response.getString("date_of_discharge").equals("null") ? "" : response.getString("date_of_discharge"));
            mDischargeWeight.setText(response.getString("discharge_weight").equals("null") ? "" : response.getString("discharge_weight"));
            mDischargeCirc.setText(response.getString("discharge_circ").equals("null") ? "" : response.getString("discharge_circ"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goback_tv:
                finish();
                break;
            case R.id.save_btn:
                saveDetails();
                break;
        }
    }

    private void saveDetails() {
        String mUrl = getString(R.string.get_discharge_info) + childId;
        requestBody = new HashMap<String, String>();
        requestBody.put("birth_details_id", String.valueOf(birthDetailsId));
        requestBody.put("post_partum_complications", mPostPartum.getText().toString());
        requestBody.put("feeding_at_discharge", mFeeding.getText().toString());
        requestBody.put("difficulties_with_feeding", mDifficulties.getText().toString());
        requestBody.put("date_of_discharge", mDischargeDate.getText().toString());
        requestBody.put("discharge_weight", mDischargeWeight.getText().toString());
        requestBody.put("discharge_circ", mDischargeCirc.getText().toString());

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(DischargeInformationActivity.this, "Data saved.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }
}
