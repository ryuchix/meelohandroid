package com.meeloh.meeloh.Patient.BirthDetails;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.Patient.BirthDetails.NewbornExam.NewbornExaminationActivity;
import com.meeloh.meeloh.R;

import org.json.JSONException;
import org.json.JSONObject;

public class PatientBirthDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private User mUser;
    private Integer childId;
    private LinearLayout emptyBirthDetailsLL, birthDetailsLL;
    private TextView mName, mFacility, mBirthDate, mBirthTime, mGender, mNewborn;
    private Button mFillOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_birth_details);

        childId = getIntent().getIntExtra("children_id", 0);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Birth details");
        mToolbar.setNavigationIcon(R.drawable.arrow_back_24px);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        emptyBirthDetailsLL = findViewById(R.id.empty_birthdetails_ll);
        birthDetailsLL = findViewById(R.id.birthdetails_ll);
        mName = findViewById(R.id.child_name_tv);
        mFacility = findViewById(R.id.child_birth_facility_tv);
        mBirthDate = findViewById(R.id.child_birth_date_tv);
        mBirthTime = findViewById(R.id.child_birthtime_tv);
        mGender = findViewById(R.id.child_child_gender_tv);
        mFillOut = findViewById(R.id.fill_out_details_btn);
        mFillOut.setOnClickListener(this);
        mNewborn = findViewById(R.id.newborn_exam_tv);
        mNewborn.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        getBirthDetails();
        super.onResume();
    }

    private void getBirthDetails() {
        emptyBirthDetailsLL.setVisibility(View.VISIBLE);
        birthDetailsLL.setVisibility(View.GONE);
        String mUrl = getString(R.string.get_birth_details) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        try {
                            if (response.getBoolean("birth_status") && response.getBoolean("maternal_status") && response.getBoolean("neonatal_status") && response.getBoolean("discharge_status")) {
                                emptyBirthDetailsLL.setVisibility(View.GONE);
                                birthDetailsLL.setVisibility(View.VISIBLE);
                                setBirthDetails(response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setBirthDetails(JSONObject response) {
        try {
            mName.setText(response.getString("name"));
            mFacility.setText(response.getString("birth_facility"));
            mBirthDate.setText(response.getString("birth_date"));
            mBirthTime.setText(response.getString("birth_time"));
            mGender.setText(response.getString("gender"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fill_out_details_btn:
                openFillOut();
                break;
            case R.id.newborn_exam_tv:
                openNewborn();
                break;
        }
    }

    private void openNewborn() {
        Intent intent = new Intent(this, NewbornExaminationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("children_id", childId);
        startActivity(intent);
    }

    private void openFillOut() {
        Intent intent = new Intent(this, PatientAddBirthdetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("children_id", childId);
        startActivity(intent);
    }
}
