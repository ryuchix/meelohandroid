package com.meeloh.meeloh.Patient.BirthDetails;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.Patient.BirthDetails.Discharge.DischargeInformationActivity;
import com.meeloh.meeloh.Patient.BirthDetails.Maternal.MaternalInformationActivity;
import com.meeloh.meeloh.Patient.BirthDetails.Neonatal.NeonatalInformationActivity;
import com.meeloh.meeloh.Profile.EditDoctorProfile;
import com.meeloh.meeloh.R;
import com.meeloh.meeloh.UserAccountSetter;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class PatientAddBirthdetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private User mUser;
    private Integer childId;
    private TextInputEditText mName, mFacility, mDob, mToB, mGender;
    private String name, facility, dob, tob, gender;

    private String TAG = "EditProfileActivity";
    private LayoutInflater mInflator;
    private Spinner typeSpinner;
    private Boolean selected;
    private Button mSave;
    private HashMap<String , String > requestBody;
    private DatePickerDialog picker;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private TextView maternalStatus, neonatalStatus, dischargeStatus;
    private LinearLayout mMaternal, mNeonatal, mDischarge;
    private int birthDetailsId;
    private boolean hasDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_add_birthdetails);

        requestBody = new HashMap<>();
        childId = getIntent().getIntExtra("children_id", 0);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Birth details");
        mToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        selected = false;

        typeSpinner = (Spinner) findViewById(R.id.spinner);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(typeSelectedListener);
        typeSpinner.setOnTouchListener(typeSpinnerTouchListener);

        mInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        mName = findViewById(R.id.given_name_tiet);
        mFacility = findViewById(R.id.birth_facility_tiet);
        mDob = findViewById(R.id.date_of_birthdate_tiet);
        mDob.setOnClickListener(this);
        mToB = findViewById(R.id.date_of_birthtime_tiet);
        mToB.setOnClickListener(this);
        mSave = findViewById(R.id.save_birthdetails_btn);
        mSave.setOnClickListener(this);
        maternalStatus = findViewById(R.id.maternal_status);
        neonatalStatus = findViewById(R.id.neonatal_status);
        dischargeStatus = findViewById(R.id.discharge_status);

        mMaternal = findViewById(R.id.maternal_ll);
        mMaternal.setOnClickListener(this);
        mNeonatal = findViewById(R.id.neonatal_ll);
        mNeonatal.setOnClickListener(this);
        mDischarge = findViewById(R.id.discharge_ll);
        mDischarge.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        getBirthDetails();
        super.onResume();
    }

    private SpinnerAdapter typeSpinnerAdapter = new BaseAdapter() {
        private TextView text;
        private String[] data = { "Male", "Female" };
        private int count = 2;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);

            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return count;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            return false;
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_birthdetails_btn:
                saveDetails();
                break;
            case R.id.date_of_birthdate_tiet:
                openDatePicker();
                break;
            case R.id.date_of_birthtime_tiet:
                openTimePicker(mToB);
                break;
            case R.id.maternal_ll:
                openMaternal();
                break;
            case R.id.neonatal_ll:
                openNeonatal();
                break;
            case R.id.discharge_ll:
                openDischarge();
                break;
        }
    }

    private void openDischarge() {
        if (hasDetails) {
            Intent intent = new Intent(this, DischargeInformationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("children_id", childId);
            intent.putExtra("birth_details_id", birthDetailsId);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please save birth details first.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openNeonatal() {
        if (hasDetails) {
            Intent intent = new Intent(this, NeonatalInformationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("children_id", childId);
            intent.putExtra("birth_details_id", birthDetailsId);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please save birth details first.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openMaternal() {
        if (hasDetails) {
            Intent intent = new Intent(this, MaternalInformationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("children_id", childId);
            intent.putExtra("birth_details_id", birthDetailsId);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please save birth details first.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openTimePicker(final TextView tv) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PatientAddBirthdetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm = "";
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);
                boolean isPM = (selectedHour >= 12);
                tv.setText( String.format("%01d:%02d %s", (selectedHour == 12 || selectedHour == 0) ? 12 : selectedHour % 12, selectedMinute, isPM ? "PM" : "AM"));

            }
        }, hour, minute, false); //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void openDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        final SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        final String month_name = month_date.format(cldr.getTime());
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        picker = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDob.setText(MONTHS[monthOfYear] + " " + dayOfMonth + ", " + year);
                    }
                }, year, month, day);
        picker.show();
    }

    private void saveDetails() {
        name = mName.getText().toString();
        facility = mFacility.getText().toString();
        dob = mDob.getText().toString();
        tob = mToB.getText().toString();

        if (name.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mName.requestFocus();
        } else if (facility.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mFacility.requestFocus();
        } else if (dob.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mDob.requestFocus();
        } else if (tob.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mToB.requestFocus();
        } else {
            updateDetails();
        }
    }

    private void updateDetails() {
        String mUrl = getString(R.string.update_birth_details) + childId;
        requestBody = new HashMap<String, String>();
        requestBody.put("name", name);
        requestBody.put("birth_facility", facility);
        requestBody.put("birth_date", dob);
        requestBody.put("gender", typeSpinner.getSelectedItem().toString());
        requestBody.put("birth_time", tob);
        requestBody.put("children_id", String.valueOf(childId));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        try {
                            birthDetailsId = response.getInt("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }

    private void getBirthDetails() {
        String mUrl = getString(R.string.get_birth_details) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        try {
                            if (response.getBoolean("birth_status")) {
                                setBirthDetails(response);
                            } else {
                                hasDetails = false;
                            }
                            if (response.getBoolean("maternal_status")) {
                                maternalStatus.setText("Completed");
                            } else {
                                maternalStatus.setText("Incomplete");
                            }
                            if (response.getBoolean("neonatal_status")) {
                                neonatalStatus.setText("Completed");
                            } else {
                                neonatalStatus.setText("Incomplete");
                            }
                            if (response.getBoolean("discharge_status")) {
                                dischargeStatus.setText("Completed");
                            } else {
                                dischargeStatus.setText("Incomplete");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setBirthDetails(JSONObject response) {
        try {
            birthDetailsId =  response.getInt("id");
            hasDetails = true;

            mName.setText(response.getString("name"));
            mFacility.setText(response.getString("birth_facility"));
            mDob.setText(response.getString("birth_date"));
            mToB.setText(response.getString("birth_time"));
            String gender = response.getString("gender");

            if (gender.equals("Male")) {
                typeSpinner.setSelection(0);
            }
            if (gender.equals("Female")) {
                typeSpinner.setSelection(1);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
