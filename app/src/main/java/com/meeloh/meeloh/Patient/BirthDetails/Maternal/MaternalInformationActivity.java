package com.meeloh.meeloh.Patient.BirthDetails.Maternal;

import android.annotation.SuppressLint;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MaternalInformationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Integer childId;
    private int birthDetailsId;
    private TextView goBack;
    private String name, pregnancy, bloodroup, antiDGiven, labourSpontaneous, labourComplications, typeOfBirth, other;
    private TextInputEditText mName, mPregnancy, mBloodroup, mAntiDGiven, mLabourSpontaneous, mLabourComplications, mTypeOfBirth, mOther;
    private Button mSave;
    private HashMap<String , String > requestBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maternal_information);

        requestBody = new HashMap<>();
        childId = getIntent().getIntExtra("children_id", 0);
        birthDetailsId = getIntent().getIntExtra("birth_details_id", 0);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Birth details");
        mToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        goBack = findViewById(R.id.goback_tv);
        goBack.setOnClickListener(this);
        mName = findViewById(R.id.mothersname_tiet);
        mPregnancy = findViewById(R.id.pregnancy_complications_tiet);
        mBloodroup = findViewById(R.id.blood_group__tiet);
        mAntiDGiven = findViewById(R.id.anti_d_given_tiet);
        mLabourSpontaneous = findViewById(R.id.labour_spontaneous_tiet);
        mLabourComplications = findViewById(R.id.labour_complications_tiet);
        mTypeOfBirth = findViewById(R.id.type_of_birth_tiet);
        mOther = findViewById(R.id.other_tiet);
        mSave = findViewById(R.id.save_btn);
        mSave.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        getMaternalInfo();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void getMaternalInfo() {
        String mUrl = getString(R.string.get_maternal_info) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        if (response.length() > 0) {
                            setMaternalInfo(response);
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setMaternalInfo(JSONObject response) {
        try {
            mName.setText(response.getString("mothers_name"));
            mPregnancy.setText(response.getString("pregnancy_complications"));
            mBloodroup.setText(response.getString("blood_group"));
            mAntiDGiven.setText(response.getString("anti_d_given"));
            mLabourSpontaneous.setText(response.getString("labour_spontaneous"));
            mLabourComplications.setText(response.getString("labour_complications"));
            mTypeOfBirth.setText(response.getString("type_of_birth"));
            mOther.setText(response.getString("other"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goback_tv:
                finish();
                break;
            case R.id.save_btn:
                saveDetails();
                break;
        }
    }

    private void saveDetails() {
        name = mName.getText().toString();
        pregnancy = mPregnancy.getText().toString();
        bloodroup = mBloodroup.getText().toString();
        antiDGiven = mAntiDGiven.getText().toString();
        labourSpontaneous = mLabourSpontaneous.getText().toString();
        labourComplications = mLabourComplications.getText().toString();
        typeOfBirth = mTypeOfBirth.getText().toString();
        other = mOther.getText().toString();

        if (name.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mName.requestFocus();
        } else if (pregnancy.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mPregnancy.requestFocus();
        } else if (bloodroup.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mBloodroup.requestFocus();
        } else if (antiDGiven.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mAntiDGiven.requestFocus();
        } else if (labourSpontaneous.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLabourSpontaneous.requestFocus();
        } else if (labourComplications.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mLabourComplications.requestFocus();
        } else if (typeOfBirth.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mTypeOfBirth.requestFocus();
        }  else if (other.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
            mOther.requestFocus();
        } else {
            updateDetails();
        }
    }

    private void updateDetails() {
        String mUrl = getString(R.string.get_maternal_info) + childId;
        requestBody = new HashMap<String, String>();
        requestBody.put("mothers_name", name);
        requestBody.put("pregnancy_complications", pregnancy);
        requestBody.put("blood_group", bloodroup);
        requestBody.put("anti_d_given", antiDGiven);
        requestBody.put("labour_spontaneous", labourSpontaneous);
        requestBody.put("labour_complications", labourComplications);
        requestBody.put("type_of_birth", typeOfBirth);
        requestBody.put("other", other);
        requestBody.put("birth_details_id", String.valueOf(birthDetailsId));

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(MaternalInformationActivity.this, "Data saved.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }
}
