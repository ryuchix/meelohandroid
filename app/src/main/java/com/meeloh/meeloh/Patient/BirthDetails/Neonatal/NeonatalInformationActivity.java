package com.meeloh.meeloh.Patient.BirthDetails.Neonatal;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.util.HashMap;

public class NeonatalInformationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private Integer childId;
    private int birthDetailsId;
    private TextView goBack;
    private String name, pregnancy, bloodroup, antiDGiven, labourSpontaneous, labourComplications, typeOfBirth, other;
    private TextInputEditText mNewbornBloodspotDate, mHepBImmunisationDate, mHepBImmunogoblinDate;
    private TextInputLayout mNewbornBloodspotDateLayout, mHepBImmunisationLayout, mHepBImmunoGoblinLayout;
    private Button mSave;
    private HashMap<String , String > requestBody;
    private String[] vitamink;
    private LinearLayout dose1, dose2, dose3;
    private CheckBox newbornBloodSpot, hepBImmunisation, hepBImmunoGoblin, newbornHearing;

    private LayoutInflater mInflator;
    private Spinner typeSpinner;
    private Boolean selected;
    private TextInputEditText estimated_gestation_tiet, apgar_1_min_tiet, apgar_5_min_tiet, abnormalities_noted_at_birth_tiet, problems_requiring_treatment_tiet,
            birth_weight_tiet, birth_length_tiet, birth_head_tiet, others_tiet, date_tiet,_1st_dose_date_tiet, _2nd_dose_date_tiet, _3rd_dose_date_tiet;
    private boolean isLoaded = false;
    private String loadedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neonatal);

        requestBody = new HashMap<>();
        childId = getIntent().getIntExtra("children_id", 0);
        birthDetailsId = getIntent().getIntExtra("birth_details_id", 0);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Birth details");
        mToolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        selected = false;
        typeSpinner = (Spinner) findViewById(R.id.vitamin_k_given_tiet);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(typeSelectedListener);
        typeSpinner.setOnTouchListener(typeSpinnerTouchListener);
        mInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        dose1 = findViewById(R.id.dose1_ll);
        dose2 = findViewById(R.id.dose2_ll);
        dose3 = findViewById(R.id.dose3_ll);
        dose1.setVisibility(View.GONE);
        dose2.setVisibility(View.GONE);
        dose3.setVisibility(View.GONE);
        newbornBloodSpot = findViewById(R.id.newborn_bloodspot_date_cb);
        mNewbornBloodspotDate = findViewById(R.id.newborn_bloodspot_date_tiet);
        mNewbornBloodspotDateLayout = findViewById(R.id.newborn_bloodspot_date_layout);
        mNewbornBloodspotDateLayout.setVisibility(View.GONE);

        hepBImmunisation = findViewById(R.id.hep_b_immunisation_given);
        mHepBImmunisationDate = findViewById(R.id.hep_b_immunisation_date_tiet);
        mHepBImmunisationLayout = findViewById(R.id.hep_b_immunisation_date_layout);
        mHepBImmunisationLayout.setVisibility(View.GONE);

        hepBImmunoGoblin = findViewById(R.id.hep_b_immunogoblin_given);
        mHepBImmunogoblinDate = findViewById(R.id.hep_b_immunogoblin_date_tiet);
        mHepBImmunoGoblinLayout = findViewById(R.id.hep_b_immunogoblin_date_layout);
        mHepBImmunoGoblinLayout.setVisibility(View.GONE);

        newbornHearing = findViewById(R.id.newborn_hearing_screening);

        estimated_gestation_tiet = findViewById(R.id.estimated_gestation_tiet);
        apgar_1_min_tiet = findViewById(R.id.apgar_1_min_tiet);
        apgar_5_min_tiet = findViewById(R.id.apgar_5_min_tiet);
        abnormalities_noted_at_birth_tiet = findViewById(R.id.abnormalities_noted_at_birth_tiet);
        problems_requiring_treatment_tiet = findViewById(R.id.problems_requiring_treatment_tiet);
        birth_weight_tiet = findViewById(R.id.birth_weight_tiet);
        birth_length_tiet = findViewById(R.id.birth_length_tiet);
        birth_head_tiet = findViewById(R.id.birth_head_tiet);
        others_tiet = findViewById(R.id.others_tiet);
        date_tiet = findViewById(R.id.date_tiet);
        _1st_dose_date_tiet = findViewById(R.id._1st_dose_date_tiet);
        _2nd_dose_date_tiet = findViewById(R.id._2nd_dose_date_tiet);
        _3rd_dose_date_tiet = findViewById(R.id._3rd_dose_date_tiet);

        mSave = findViewById(R.id.save_btn);
        mSave.setOnClickListener(this);
        goBack = findViewById(R.id.goback_tv);
        goBack.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        getNeonatal();
        super.onResume();
    }

    private void getNeonatal() {
        String mUrl = getString(R.string.get_neonatal_info) + childId;
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        if (response.length() > 0) {
                            setNeonatal(response);
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }

    private void setNeonatal(JSONObject response) {
        try {
            estimated_gestation_tiet.setText(response.getString("estimate_gestation").equals("null") ? "" : response.getString("estimate_gestation"));
            apgar_1_min_tiet.setText(response.getString("apgar_1_minute").equals("null") ? "" : response.getString("apgar_1_minute"));
            apgar_5_min_tiet.setText(response.getString("apgar_5_minute").equals("null") ? "" : response.getString("apgar_5_minute"));
            abnormalities_noted_at_birth_tiet.setText(response.getString("abnormalities_noted_a_birth").equals("null") ? "" : response.getString("abnormalities_noted_a_birth"));
            problems_requiring_treatment_tiet.setText(response.getString("problems_requiring_treatment").equals("null") ? "" : response.getString("problems_requiring_treatment"));
            birth_weight_tiet.setText(response.getString("birth_weight").equals("null") ? "" : response.getString("birth_weight"));
            birth_length_tiet.setText(response.getString("birth_length").equals("null") ? "" : response.getString("birth_length"));
            birth_head_tiet.setText(response.getString("birth_head").equals("null") ? "" : response.getString("birth_head"));

            if (response.getInt("newborn_hearing_screen_completed") == 1) {
                newbornHearing.setChecked(true);
            } else {
                newbornHearing.setChecked(false);
            }

            if (response.getInt("newborn_bloodspot_screen_completed") == 1) {
                newbornBloodSpot.setChecked(true);
                mNewbornBloodspotDateLayout.setVisibility(View.VISIBLE);
                mNewbornBloodspotDate.setText(response.getString("newborn_bloodspot_screen_date_completed").equals("null") ? "" : response.getString("newborn_bloodspot_screen_date_completed"));
            } else {
                newbornBloodSpot.setChecked(false);
                mNewbornBloodspotDateLayout.setVisibility(View.VISIBLE);
                mNewbornBloodspotDate.setText(response.getString("newborn_bloodspot_screen_date_completed").equals("null") ? "" : response.getString("newborn_bloodspot_screen_date_completed"));
            }

            mNewbornBloodspotDate.setText(response.getString("newborn_bloodspot_screen_date_completed").equals("null") ? "" : response.getString("newborn_bloodspot_screen_date_completed"));

            others_tiet.setText(response.getString("other").equals("null") ? "" : response.getString("other"));
            date_tiet.setText(response.getString("date").equals("null") ? "" : response.getString("date"));

            typeSpinner.setAdapter(typeSpinnerAdapter);

            if (response.getString("vitamin_k_given").equals("Oral")) {
                isLoaded = true;
                loadedValue = "Oral";
                dose1.setVisibility(View.VISIBLE);
                dose2.setVisibility(View.VISIBLE);
                dose3.setVisibility(View.VISIBLE);
            }
            if (response.getString("vitamin_k_given").equals("Injection")) {
                isLoaded = true;
                loadedValue = "Injection";
                dose1.setVisibility(View.VISIBLE);
                dose2.setVisibility(View.VISIBLE);
                dose3.setVisibility(View.VISIBLE);
            }
            if (response.getString("vitamin_k_given").equals("NA")) {
                isLoaded = true;
                loadedValue = "NA";;
                dose1.setVisibility(View.GONE);
                dose2.setVisibility(View.GONE);
                dose3.setVisibility(View.GONE);
            }
            if (response.getString("vitamin_k_given").equals("null")) {
                isLoaded = false;
                dose1.setVisibility(View.GONE);
                dose2.setVisibility(View.GONE);
                dose3.setVisibility(View.GONE);
            }

            _1st_dose_date_tiet.setText(response.getString("vitamin_k_1st_dose").equals("null") ? "" : response.getString("vitamin_k_1st_dose"));
            _2nd_dose_date_tiet.setText(response.getString("vitamin_k_2nd_dose").equals("null") ? "" : response.getString("vitamin_k_2nd_dose"));
            _3rd_dose_date_tiet.setText(response.getString("vitamin_k_3rd_dose").equals("null") ? "" : response.getString("vitamin_k_3rd_dose"));

            if (response.getInt("hep_b_immunisation_given") == 1) {
                hepBImmunisation.setChecked(true);
                mHepBImmunisationLayout.setVisibility(View.VISIBLE);
                mHepBImmunisationDate.setText(response.getString("hep_b_immunisation_date_given").equals("null") ? "" : response.getString("hep_b_immunisation_date_given"));
            } else {
                hepBImmunisation.setChecked(false);
                mHepBImmunisationLayout.setVisibility(View.GONE);
            }

            if (response.getInt("hep_b_immunogoboblin") == 1) {
                hepBImmunoGoblin.setChecked(true);
                mHepBImmunoGoblinLayout.setVisibility(View.VISIBLE);
                mHepBImmunogoblinDate.setText(response.getString("hep_b_immunogoboblin_date").equals("null") ? "" : response.getString("hep_b_immunogoboblin_date"));
            } else {
                hepBImmunoGoblin.setChecked(false);
                mHepBImmunoGoblinLayout.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkbox_clicked(View v) {
        if (newbornBloodSpot.isChecked()) {
            mNewbornBloodspotDateLayout.setVisibility(View.VISIBLE);
        } else {
            mNewbornBloodspotDateLayout.setVisibility(View.GONE);
        }
        if (hepBImmunisation.isChecked()) {
            mHepBImmunisationLayout.setVisibility(View.VISIBLE);
        } else {
            mHepBImmunisationLayout.setVisibility(View.GONE);
        }
        if (hepBImmunoGoblin.isChecked()) {
            mHepBImmunoGoblinLayout.setVisibility(View.VISIBLE);
        } else {
            mHepBImmunoGoblinLayout.setVisibility(View.GONE);
        }
    }

        private SpinnerAdapter typeSpinnerAdapter = new BaseAdapter() {
        private TextView text;
        private String[] data = { "NA", "Injection", "Oral"};
        private int count = 3;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.row_spinner, null);
            }
            text = (TextView) convertView.findViewById(R.id.spinnerTarget);
            if (selected) {
                text.setText(data[position]);
                text.setTextColor(Color.BLACK);
                text.setTextSize(16);
                text.setPadding(40, 0, 20, 0);
            } else {
                text.setText("Vitamin K given");
                text.setTextSize(16);
                text.setTextColor(Color.parseColor("#808080"));
            }
            if (position == 0) {
                dose1.setVisibility(View.GONE);
                dose2.setVisibility(View.GONE);
                dose3.setVisibility(View.GONE);
                ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            } else {
                dose1.setVisibility(View.VISIBLE);
                dose2.setVisibility(View.VISIBLE);
                dose3.setVisibility(View.VISIBLE);
                ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            }
            if (isLoaded) {
                text.setText(loadedValue);
                text.setTextColor(Color.BLACK);
                text.setTextSize(16);
                dose1.setVisibility(View.VISIBLE);
                dose2.setVisibility(View.VISIBLE);
                dose3.setVisibility(View.VISIBLE);
            }

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public int getCount() {
            return 3;
        }

        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflator.inflate(
                        android.R.layout.simple_spinner_dropdown_item, null);
            }
            text = (TextView) convertView.findViewById(android.R.id.text1);
            text.setText(data[position]);
            text.setTextColor(Color.BLACK);
            text.setPadding(40, 30, 20, 30);
            return convertView;
        };
    };

    private AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnTouchListener typeSpinnerTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            selected = true;
            isLoaded = false;
            ((BaseAdapter) typeSpinnerAdapter).notifyDataSetChanged();
            dose1.setVisibility(View.VISIBLE);
            dose2.setVisibility(View.VISIBLE);
            dose3.setVisibility(View.VISIBLE);
            return false;
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_btn:
                saveDetails();
                break;
            case R.id.goback_tv:
                finish();
                break;
        }
    }

    private void saveDetails() {
        String mUrl = getString(R.string.get_neonatal_info) + childId;
        requestBody = new HashMap<String, String>();
        requestBody.put("birth_details_id", String.valueOf(birthDetailsId));
        requestBody.put("estimate_gestation", estimated_gestation_tiet.getText().toString());
        requestBody.put("apgar_1_minute", apgar_1_min_tiet.getText().toString());
        requestBody.put("apgar_5_minute", apgar_5_min_tiet.getText().toString());
        requestBody.put("abnormalities_noted_a_birth", abnormalities_noted_at_birth_tiet.getText().toString());
        requestBody.put("problems_requiring_treatment", problems_requiring_treatment_tiet.getText().toString());
        requestBody.put("birth_weight", birth_weight_tiet.getText().toString());
        requestBody.put("birth_length", birth_length_tiet.getText().toString());
        requestBody.put("birth_head", birth_head_tiet.getText().toString());
        requestBody.put("newborn_hearing_screen_completed", String.valueOf(newbornHearing.isChecked() ? 1 : 0));
        requestBody.put("newborn_bloodspot_screen_completed", String.valueOf(newbornBloodSpot.isChecked() ? 1 : 0));
        requestBody.put("newborn_bloodspot_screen_date_completed", mNewbornBloodspotDate.getText().toString());
        requestBody.put("other", others_tiet.getText().toString());
        requestBody.put("date", date_tiet.getText().toString());
        requestBody.put("vitamin_k_given", typeSpinner.getSelectedItem().toString());
        requestBody.put("vitamin_k_1st_dose", _1st_dose_date_tiet.getText().toString());
        requestBody.put("vitamin_k_2nd_dose", _2nd_dose_date_tiet.getText().toString());
        requestBody.put("vitamin_k_3rd_dose", _3rd_dose_date_tiet.getText().toString());
        requestBody.put("hep_b_immunisation_given", String.valueOf(hepBImmunisation.isChecked() ? 1 : 0));
        requestBody.put("hep_b_immunisation_date_given", mHepBImmunisationDate.getText().toString());
        requestBody.put("hep_b_immunogoboblin", String.valueOf(hepBImmunoGoblin.isChecked() ? 1 : 0));
        requestBody.put("hep_b_immunogoboblin_date", mHepBImmunogoblinDate.getText().toString());

        new SendRequest(this).JsonPostRequestWithAuth(mUrl, new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        Toast.makeText(NeonatalInformationActivity.this, "Data saved.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                        //
                    }
                });
    }
}
