package com.meeloh.meeloh;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.meeloh.meeloh.Home.Fragments.Calendar.CalendarFragment;
import com.meeloh.meeloh.Home.Fragments.Children.ChildrenFragment;
import com.meeloh.meeloh.Home.Fragments.Patient.PatientFragment;
import com.meeloh.meeloh.Home.Fragments.Profile.DoctorProfileFragment;
import com.meeloh.meeloh.Home.Fragments.Profile.ProfileFragment;
import com.meeloh.meeloh.Children.AddChildActivity;
import com.meeloh.meeloh.Models.User;
import com.meeloh.meeloh.NetworkUtils.SendRequest;
import com.meeloh.meeloh.NetworkUtils.VolleyJsonPostRequestCallback;
import com.meeloh.meeloh.Doctor.DoctorList.Patient.AddPatientActivity;
import com.meeloh.meeloh.Profile.EditDoctorProfile;
import com.meeloh.meeloh.Profile.EditProfileActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private FloatingActionButton addChild;
    private TextView toolbarTitle, mName;
    private boolean showToolbarRightIcon = false;
    private Toolbar toolbar;
    private BottomNavigationView navigation;
    private NavigationView navigationView;

    private User mUser;
    private static final String PASSPORT_TOKEN_KEY = "API_PASSPORT_TOKEN";
    private SharedPreferences mSharedPreferences;

    private CircleImageView mImage;
    private ImageView cImage;
    private MenuItem itemToHide;
    private MenuItem itemToShow;

    @SuppressLint({"RestrictedApi", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mUser = AccountsManager.getInstance().getUser();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        addChild = findViewById(R.id.add_fab);
        addChild.setOnClickListener(this);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);

        if (mUser.getUsertype() == 1) {
            navigation.getMenu().clear();
            navigation.inflateMenu(R.menu.navigation);
        } else {
            navigation.getMenu().clear();
            navigation.inflateMenu(R.menu.doctor_menu);
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.profile_menu);

        mName = navigationView.getHeaderView(0)
                .findViewById(R.id.name_text);
        mImage = navigationView.getHeaderView(0)
                .findViewById(R.id.profile_image);
        cImage = navigationView.getHeaderView(0)
                .findViewById(R.id.profile_image_iv);

        Bundle extras = getIntent().getExtras();
        int tab;

        if (extras != null) {
            tab = extras.getInt("tab");

            if (tab == 1) {
                navigation.setSelectedItemId(R.id.children_menu);
            } else if (tab == 2) {
                navigation.setSelectedItemId(R.id.calendar_menu);
            } else if (tab == 3) {
                navigation.setSelectedItemId(R.id.profile_menu);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuItem item = navigation.getMenu().findItem(navigation.getSelectedItemId());

        switch (item.getItemId()) {
            case R.id.children_menu:
                getSupportActionBar().setTitle("Children");
                getMenuInflater().inflate(R.menu.empty, menu);
                return true;
            case R.id.patients_menu:
                getSupportActionBar().setTitle("Patients");
                getMenuInflater().inflate(R.menu.search_menu, menu);
                return true;
            case R.id.calendar_menu:
                getSupportActionBar().setTitle("Calendar");
                getMenuInflater().inflate(R.menu.main, menu);
                return true;
            case R.id.profile_menu:
                getSupportActionBar().setTitle("Profile");
                getMenuInflater().inflate(R.menu.edit_profile_menu, menu);
                return true;
            default:
                return true;
        }

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem currentBottomNavItem =
                navigation.getMenu().findItem(navigation.getSelectedItemId());

        switch (currentBottomNavItem.getItemId()) {
            case R.id.children_menu:
                toolbar.getMenu().clear();
                getMenuInflater().inflate(R.menu.empty, menu);
                return true;
            case R.id.patients_menu:
                toolbar.getMenu().clear();
                getMenuInflater().inflate(R.menu.search_menu, menu);
                return true;
            case R.id.calendar_menu:
                toolbar.getMenu().clear();
                getMenuInflater().inflate(R.menu.main, menu);
                return true;
            case R.id.profile_menu:
                toolbar.getMenu().clear();
                getMenuInflater().inflate(R.menu.edit_profile_menu, menu);
                return true;
            default:
                return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.update_profile_menu) {
            if (mUser.getUsertype() == 1) {
                Intent intent = new Intent(this, EditProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, EditDoctorProfile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.notification) {
            // Handle the camera action
        } else if (id == R.id.family) {

        } else if (id == R.id.list_of_doctors) {

        } else if (id == R.id.settings) {

        } else if (id == R.id.signout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        HashMap<String, String> requestBody = new HashMap<>();

        new SendRequest(this).JsonPostRequestWithAuth(
                getString(R.string.logout_url),
                new JSONObject(requestBody),
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE)
                                .edit().clear().commit();
                        AccountsManager.getInstance().setUser(null);
                        Intent intent = new Intent(getApplicationContext(), LandingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finishAffinity();
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {
                    }
                });

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressLint("RestrictedApi")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.children_menu:
                    invalidateOptionsMenu();
                    item.setTitle("Children");
                    toolbarTitle.setText("Children");
                    fragment = new ChildrenFragment();
                    loadFragment(fragment);
                    addChild.setVisibility(View.VISIBLE);
                    return true;
                case R.id.patients_menu:
                    invalidateOptionsMenu();
                    item.setTitle("Patients");
                    toolbarTitle.setText("Patients");
                    fragment = new PatientFragment();
                    loadFragment(fragment);
                    addChild.setVisibility(View.VISIBLE);
                    return true;
                case R.id.calendar_menu:
                    invalidateOptionsMenu();
                    item.setTitle("Calendar");
                    toolbarTitle.setText("Calendar");
                    fragment = new CalendarFragment();
                    loadFragment(fragment);
                    addChild.setVisibility(View.GONE);
                    return true;
                case R.id.profile_menu:
                    invalidateOptionsMenu();
                    item.setTitle("Profile");
                    toolbarTitle.setText("Profile");
                    if (mUser.getUsertype() == 1) {
                        fragment = new ProfileFragment();
                        loadFragment(fragment);
                    } else {
                        fragment = new DoctorProfileFragment();
                        loadFragment(fragment);
                    }
                    addChild.setVisibility(View.GONE);
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_fab:
                if (mUser.getUsertype() == 1) {
                    openAddChild();
                } else {
                    openAddPatient();
                }
                break;
        }
    }

    private void openAddPatient() {
        Intent intent = new Intent(this, AddPatientActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void openAddChild() {
        Intent intent = new Intent(this, AddChildActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        mName.setText(mUser.getmFirstName() + " " + mUser.getmLastName());
        getProfile();

        super.onResume();
    }

    private void getProfile() {
        String mUrl = getString(R.string.get_user_details_url) + mUser.getId();
        new SendRequest(this).JsonGetRequest(
                mUrl,
                new VolleyJsonPostRequestCallback() {
                    @Override
                    public void onJsonPostRequestSuccess(JSONObject response) {
                        String image;
                        JSONObject doctor = response.optJSONObject("doctor");
                        try {
                            if (mUser.getUsertype() == 1) {
                                image = response.getString("image");
                            } else {
                                image = doctor.getString("image");
                            }
                            new UserAccountSetter().updateImage(image);

                            Picasso.get().load(image).placeholder(R.drawable.user_default).into(mImage);
                            if (mUser.getImage() != null) {
                                cImage.setVisibility(View.INVISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onJsonPostRequestError(VolleyError error) {}

                });
    }
}
